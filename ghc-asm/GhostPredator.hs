module GhostPredator where

import Prelude hiding (Right, Left)

import GHC.Types
import GHC.Assembler


ghostPredator :: AsmGHC ()
ghostPredator = do
    getManCoords
    mov (Reg E) (Var $ Reg A)           -- save Man x in E
    mov (Reg F) (Var $ Reg B)           -- save Man y in F
    getSelfCoords
    mov (Reg G) (Var $ Reg A)           -- save self x in G
    mov (Reg H) (Var $ Reg B)           -- save self y in H
    label "predator7"
    jlt "predator9" (Var $ Reg E) (Var $ Reg G)   -- line 7
    jgt "predator17" (Var $ Reg E) (Var $ Reg G)
    label "predator9"
    mov (Reg A) (Var $ Reg G)           -- line 9 check if can go left
    mov (Reg B) (Var $ Reg H)
    label "predator11"
    dec (Reg A)                         -- line 11
    getMapCell
    jlt "predator25" (Var $ Reg A) (Val 1)        -- it's wall on the left, check another direction
    setDirection Left
    label "predator16"
    hlt                                 -- line 16
    label "predator17"
    mov (Reg A) (Var $ Reg G)           -- line 17 check if can go right
    mov (Reg B) (Var $ Reg H)
    inc (Reg A)
    getMapCell
    jlt "predator25" (Var $ Reg A) (Val 1)        -- it's wall on the right, check another direction
    setDirection Right
    label "predator24"
    hlt                                 -- line 24

    label "predator25"
    jlt "predator27" (Var $ Reg F) (Var $ Reg H)  -- line 25
    jgt "predator35" (Var $ Reg F) (Var $ Reg H)
    label "predator27"
    mov (Reg A) (Var $ Reg G)           -- line 27 check if can go up
    mov (Reg B) (Var $ Reg H)
    dec (Reg B)
    getMapCell
    jlt "predator43" (Var $ Reg A) (Val 1)        -- it's wall above, check another direction
    setDirection Up
    label "predator34"
    hlt                                 -- line 34
    label "predator35"
    mov (Reg A) (Var $ Reg G)           -- line 35 check if can go down
    mov (Reg B) (Var $ Reg H)
    inc (Reg B)
    getMapCell
    jlt "predator43" (Var $ Reg A) (Val 1)        -- it's wall below, check another direction
    setDirection Down
    label "predator42"
    hlt                                 -- line 42

    label "predator43"
    keepDirIfCanOrTakeRandom 43         -- line 43
    hlt


