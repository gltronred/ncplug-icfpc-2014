module GHC.Types where

data Register = A | B | C | D | E | F | G | H deriving (Eq, Ord, Enum, Show)
data Addr = Reg Register | Addr Int | Ref Register deriving (Eq)
data Value = Var Addr | Val Int

data GHCcommand tag = Mov Addr Value
                    | Inc Addr
                    | Dec Addr
                    | Add Addr Value
                    | Sub Addr Value
                    | Mul Addr Value
                    | Div Addr Value
                    | And Addr Value
                    | Or  Addr Value
                    | Xor Addr Value
                    | Jlt tag Value Value
                    | Jeq tag Value Value
                    | Jgt tag Value Value
                    | Intr Int
                    | Hlt

instance Show Addr where
    show (Reg r)  = show r
    show (Addr i) = "[" ++ (show i) ++ "]"
    show (Ref r)  = "[" ++ (show r) ++ "]"

instance Show Value where
    show (Var a) = show a
    show (Val i) = show i

instance Show tag => Show (GHCcommand tag) where
    show (Mov a v)      = "MOV " ++ (show a) ++ ", " ++ (show v)
    show (Inc a)        = "INC " ++ (show a)
    show (Dec a)        = "DEC " ++ (show a)
    show (Add a v)      = "ADD " ++ (show a) ++ ", " ++ (show v)
    show (Sub a v)      = "SUB " ++ (show a) ++ ", " ++ (show v)
    show (Mul a v)      = "MUL " ++ (show a) ++ ", " ++ (show v)
    show (Div a v)      = "DIV " ++ (show a) ++ ", " ++ (show v)
    show (And a v)      = "AND " ++ (show a) ++ ", " ++ (show v)
    show (Or a v)       = "OR  " ++ (show a) ++ ", " ++ (show v)
    show (Xor a v)      = "XOR " ++ (show a) ++ ", " ++ (show v)
    show (Jlt tag a v)  = "JLT " ++ (show tag) ++ ", " ++ (show a) ++ ", " ++ (show v)
    show (Jeq tag a v)  = "JEQ " ++ (show tag) ++ ", " ++ (show a) ++ ", " ++ (show v)
    show (Jgt tag a v)  = "JGT " ++ (show tag) ++ ", " ++ (show a) ++ ", " ++ (show v)
    show (Intr i)       = "INT " ++ (show i)
    show (Hlt )         = "HLT"

data GHCLine tag = GHCLabel tag
                 | GHCCmd (GHCcommand tag)

type GHCProgram = [GHCcommand Int]
type GHCCode = [GHCLine String]

