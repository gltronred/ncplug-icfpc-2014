module GHC.Assembler where

import GHC.Types

import Control.Monad.Writer
import Data.List (foldl',lookup)
import Data.Maybe (fromJust)
import Prelude hiding (div, and, or, Right, Left)


type AsmGHC = Writer [GHCLine String]

mov :: Addr -> Value -> AsmGHC ()
mov a v = tell [GHCCmd $ Mov a v]

inc :: Addr -> AsmGHC ()
inc a = tell [GHCCmd $ Inc a]

dec :: Addr -> AsmGHC ()
dec a = tell [GHCCmd $ Dec a]

add :: Addr -> Value -> AsmGHC ()
add a v = tell [GHCCmd $ Add a v]

sub :: Addr -> Value -> AsmGHC ()
sub a v = tell [GHCCmd $ Sub a v]

mul :: Addr -> Value -> AsmGHC ()
mul a v = tell [GHCCmd $ Mul a v]

div :: Addr -> Value -> AsmGHC ()
div a v = tell [GHCCmd $ Div a v]

and :: Addr -> Value -> AsmGHC ()
and a v = tell [GHCCmd $ And a v]

or :: Addr -> Value -> AsmGHC ()
or a v = tell [GHCCmd $ Or a v]

xor :: Addr -> Value -> AsmGHC ()
xor a v = tell [GHCCmd $ Xor a v]

jlt :: String -> Value -> Value -> AsmGHC ()
jlt t v1 v2 = tell [GHCCmd $ Jlt t v1 v2]

jeq :: String -> Value -> Value -> AsmGHC ()
jeq t v1 v2 = tell [GHCCmd $ Jeq t v1 v2]

jgt :: String -> Value -> Value -> AsmGHC ()
jgt t v1 v2 = tell [GHCCmd $ Jgt t v1 v2]

int :: Int -> AsmGHC ()
int i = tell $ [GHCCmd $ Intr i]

hlt :: AsmGHC ()
hlt = tell $ [GHCCmd $ Hlt]


label :: String -> AsmGHC ()
label s = tell $ [GHCLabel s]


{-| Assembling like
> mapM_ print $ execWriter miner
MOV A, 2
INT 0
HLT
-}

-- always go down
miner :: AsmGHC ()
miner = do
    mov (Reg A) (Val 2)
    int 0
    hlt

flipper :: AsmGHC ()
flipper = do
    int 3                        --; Get our ghost index in A.
    int 5                        --; Get our x-ordinate in A.
    and (Reg A) (Val 1)          --; Zero all but least significant bit of A.
                                 --; Now A is 0 if x-ordinate is even, or 1 if it is odd.
    mov (Reg B) (Var $ Reg A)    --; Save A in B because we need to use A to set direction.
    mov (Reg A) (Val 2)          --; Move down by default.
    jeq "flipper7" (Var $ Reg B) (Val 1)  --; Don't change anything if x-ordinate is odd.
    mov (Reg A) (Val 0)          --; We only get here if x-ordinate was even, so move up.

    label "flipper7"
    int 0                        --; This is line 7, the target of the above jump. Now actually set the direction.
    hlt


{-
MM      MM        MM           MMMM      MMMMMM         MMMM   
MMM    MMM      MM  MM       MM    MM    MM    MM     MM    MM 
MMMM  MMMM      MM  MM      MM           MM    MM    MM      MM
MM MMMM MM     MM    MM     MM           MMMMMM      MM      MM
MM  MM  MM     MM    MM     MM           MM  MM      MM      MM
MM      MM    MMMMMMMMMM     MM    MM    MM   MM      MM    MM 
MM      MM    MM      MM       MMMM      MM    MM       MMMM   
-}

jmp :: String -> AsmGHC ()
jmp addr = do
    jeq addr (Val 0) (Val 0)


data Dir = Up | Right | Down | Left deriving (Eq, Ord, Enum)

{-| rewrites reg A -}
setDirection :: Dir -> AsmGHC ()
setDirection d = do
    mov (Reg A) (Val $ fromEnum d)
    int 0

{-| x coord in reg A, y coord in reg B -}
getManCoords :: AsmGHC ()
getManCoords = do
    int 1

{-| x coord in reg A, y coord in reg B -}
getSelfCoords :: AsmGHC ()
getSelfCoords = do
    int 3
    int 5

{-| vitality in reg A, direction in reg B -}
getSelfStats :: AsmGHC ()
getSelfStats = do
    int 3
    int 6

{-| x coord in reg A, y coord in reg B
    vitality in reg C, direction in reg D
-}
getSelfParams :: AsmGHC ()
getSelfParams = do
    int 3
    int 6
    mov (Reg C) (Var $ Reg A)
    mov (Reg D) (Var $ Reg B)
    int 3
    int 5

{-| In: x coord in reg A, y coord in reg B
    Out: cell type in reg A
-}
getMapCell :: AsmGHC ()
getMapCell = int 7

{-| uses reg B, cell type in reg A -}
retrMapCell :: Int -> Int -> AsmGHC ()
retrMapCell x y = do
    mov (Reg A) (Val x)
    mov (Reg B) (Val y)
    int 7

keepDirIfCanOrTakeRandom :: Int -> AsmGHC ()
keepDirIfCanOrTakeRandom offset = do
    getSelfStats
    label "kdicotr2"
    mov (Reg E) (Var $ Reg B)                -- line 2 save dir
    getSelfCoords
    label "kdicotr5"
    jgt "kdicotr30" (Var $ Reg E) (Val 2)  -- line 5, moving left
    jgt "kdicotr23" (Var $ Reg E) (Val 1)  -- moving down
    jgt "kdicotr15" (Var $ Reg E) (Val 0)  -- moving right

    label "kdicotr8"
    dec (Reg B)                              -- line 8, moving up, check if there is wall
    getMapCell
    jlt "kdicotr15" (Var $ Reg A) (Val 1)  -- it's wall above, check another direction
    setDirection Up
    label "kdicotr14"
    jmp "kdicotr36"                        -- line 14, jump out

    label "kdicotr15"
    getSelfCoords                            -- line 15, moving right, check if there is wall
    inc (Reg A)
    label "kdicotr18"
    getMapCell                               -- line 18
    jlt "kdicotr23" (Var $ Reg A) (Val 1)  -- it's wall on the right, check another direction
    setDirection Right
    label "kdicotr22"
    jmp "kdicotr36"                        -- line 22, jump out

    label "kdicotr23"
    getSelfCoords                            -- line 23, moving down, check if there is wall
    inc (Reg B)
    label "kdicotr26"
    getMapCell                               -- line 26
    jlt "kdicotr30" (Var $ Reg A) (Val 1)  -- it's wall below, check another direction
    setDirection Down
    label "kdicotr29"
    jmp "kdicotr36"                        -- line 29, jump out

    label "kdicotr30"
    getSelfCoords                            -- line 30, moving left, check if there is wall
    dec (Reg A)
    label "kdicotr33"
    getMapCell                               -- line 33
    jlt "kdicotr8" (Var $ Reg A) (Val 1)   -- it's wall on the left, check another direction
    label "kdicotr35"
    setDirection Left                        -- line 35
    label "kdicotr36"


genGHC' = execWriter
genGHC = resolveLabels . genGHC'

resolveLabels :: GHCCode -> GHCProgram
resolveLabels lines = let
  labels = snd $ foldl' collectLabel (0,[]) lines
  isCommand (GHCLabel _) = False
  isCommand (GHCCmd {}) = True
  in map (replaceLabel labels) $ filter isCommand lines

collectLabel :: (Int,[(String,Int)]) -> GHCLine String -> (Int,[(String,Int)])
collectLabel (i,labels) (GHCLabel l) = (i, (l,i):labels)
collectLabel (i,labels) (GHCCmd cmd) = (i+1, labels)

replaceLabel :: [(String,Int)] -> GHCLine String -> GHCcommand Int
replaceLabel labels (GHCCmd (Mov a v))     = Mov a v
replaceLabel labels (GHCCmd (Inc a))       = Inc a
replaceLabel labels (GHCCmd (Dec a))       = Dec a
replaceLabel labels (GHCCmd (Add a v))     = Add a v
replaceLabel labels (GHCCmd (Sub a v))     = Sub a v
replaceLabel labels (GHCCmd (Mul a v))     = Mul a v
replaceLabel labels (GHCCmd (Div a v))     = Div a v
replaceLabel labels (GHCCmd (And a v))     = And a v
replaceLabel labels (GHCCmd (Or a v))      = Or a v
replaceLabel labels (GHCCmd (Xor a v))     = Xor a v
replaceLabel labels (GHCCmd (Jlt tag a v)) = case lookup tag labels of
  Just addr -> Jlt addr a v
  Nothing -> error $ "Not found tag " ++ show tag
replaceLabel labels (GHCCmd (Jeq tag a v)) = case lookup tag labels of
  Just addr -> Jeq addr a v
  Nothing -> error $ "Not found tag " ++ show tag
replaceLabel labels (GHCCmd (Jgt tag a v)) = case lookup tag labels of
  Just addr -> Jgt addr a v
  Nothing -> error $ "Not found tag " ++ show tag
replaceLabel labels (GHCCmd (Intr i))      = Intr i
replaceLabel labels (GHCCmd (Hlt ))        = Hlt 


