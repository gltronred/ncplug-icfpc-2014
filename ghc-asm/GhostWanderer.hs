module GhostWanderer where

import Prelude hiding (Right, Left)

import GHC.Types
import GHC.Assembler


{-| Algorithm:
    - if can go right - go right
    - else if can go forward - go forward
    - else go backwards
-}
ghostWanderer :: AsmGHC ()
ghostWanderer = do
    getSelfStats
    label "wanderer2"
    mov (Reg E) (Var $ Reg B)           -- line 2 save dir in E
    getSelfCoords
    label "wanderer5"
    jgt "wanderer57" (Var $ Reg E) (Val 2)        -- line 5, moving left
    jgt "wanderer40" (Var $ Reg E) (Val 1)        -- moving down
    jgt "wanderer23" (Var $ Reg E) (Val 0)        -- moving right

    label "wanderer8"
    inc (Reg A)                         -- line 8, moving up, check if there is a wall on the right
    getMapCell
    jlt "wanderer14" (Var $ Reg A) (Val 1)        -- it's wall on the right, check forward
    setDirection Right                  -- turn right
    hlt
    label "wanderer14"
    getSelfCoords                       -- line 14, moving up, check if can do ahead
    dec (Reg B)
    getMapCell
    label "wanderer18"
    jlt "wanderer20" (Var $ Reg A) (Val 1)        -- line 18, it's wall in front of us, go back
    hlt                                 -- keep direction
    label "wanderer20"
    setDirection Down                   -- line 20
    hlt

    label "wanderer23"
    getSelfCoords                       -- line 23, moving right, check if there is wall on the right
    inc (Reg B)
    label "wanderer26"
    getMapCell                          -- line 26
    jlt "wanderer31" (Var $ Reg A) (Val 1)        -- it's wall on the right, check forward
    setDirection Down                   -- turn right
    hlt
    label "wanderer31"
    getSelfCoords                       -- line 31, moving right, check if can do ahead
    inc (Reg A)
    getMapCell
    label "wanderer35"
    jlt "wanderer37" (Var $ Reg A) (Val 1)        -- line 35, it's wall in front of us, go back
    hlt                                 -- keep direction
    label "wanderer37"
    setDirection Left                   -- line 37
    hlt

    label "wanderer40"
    getSelfCoords                       -- line 40, moving down, check if there is wall on the right
    dec (Reg A)
    label "wanderer43"
    getMapCell                          -- line 43
    jlt "wanderer48" (Var $ Reg A) (Val 1)        -- it's wall on the right, check forward
    setDirection Left                   -- turn right
    hlt
    label "wanderer48"
    getSelfCoords                       -- line 48, moving down, check if can do ahead
    inc (Reg B)
    getMapCell
    label "wanderer52"
    jlt "wanderer54" (Var $ Reg A) (Val 1)        -- line 52, it's wall in front of us, go back
    hlt                                 -- keep direction
    label "wanderer54"
    setDirection Up                     -- line 54
    hlt

    label "wanderer57"
    getSelfCoords                       -- line 57, moving left, check if there is wall on the right
    dec (Reg B)
    label "wanderer60"
    getMapCell                          -- line 60
    jlt "wanderer65" (Var $ Reg A) (Val 1)        -- it's wall on the right, check forward
    label "wanderer62"
    setDirection Up                     -- line 62
    hlt
    label "wanderer65"
    getSelfCoords                       -- line 65, moving left, check if can do ahead
    dec (Reg A)
    getMapCell
    label "wanderer69"
    jlt "wanderer71" (Var $ Reg A) (Val 1)        -- line 69, it's wall in front of us, go back
    hlt                                 -- keep direction
    label "wanderer71"
    setDirection Right                  -- line 71
    hlt

