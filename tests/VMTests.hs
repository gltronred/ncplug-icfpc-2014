module VMTests (allVMTests, counting_lambda_man) where

import GCC.Assembler
import GCC.Types
import GCCVM

import Helpers

import Test.Tasty
import Test.Tasty.HUnit

allVMTests :: [TestTree]
allVMTests = [ testMultipleSteps counting_lambda_man
             , testVM "Factorial with LD" (success [DInt 120]) factLD
             , testVM "Factorial with LDF" (success [DInt 120]) factLDF
             , testVM "Foldl with LDF" (success [DInt 15]) foldlLDF
             , testVM "Random inside recursion with LD" (success [DInt 5]) randomRecLD
             , testVM "Factorial with TCO" (success [DInt 120]) factTCO
             ]

testVM :: String -> ProgResult -> SECDProgram -> TestTree
testVM label result code = testCase label $ do
  a <- runGCC code Nothing
  assertEqual label result a

factLD :: SECDProgram
factLD = [ DUM 6
         , LDC 1
         , LDC 1103515245
         , LDC 12345
         , LDF 14
         , LDF 29
         , LDF 30
         , LDF 10
         , RAP 6
         , RTN
         , LDC 5
         , LD 0 3
         , AP 1
         , RTN
         , LDC 2
         , LD 0 0
         , CGT
         , SEL 19 21
         , RTN
         , LDC 1
         , JOIN
         , LD 0 0
         , LD 0 0
         , LDC 1
         , SUB
         , LD 1 3
         , AP 1
         , MUL
         , JOIN
         , RTN
         , LD 0 0
         , LD 0 0
         , MUL
         , RTN
         ]

factLDF :: SECDProgram
factLDF = [ DUM 6
          , LDC 1
          , LDC 1103515245
          , LDC 12345
          , LDF 14
          , LDF 29
          , LDF 30
          , LDF 10
          , RAP 6
          , RTN
          , LDC 5
          , LDF 14
          , AP 1
          , RTN
          , LDC 2
          , LD 0 0
          , CGT
          , SEL 19 21
          , RTN
          , LDC 1
          , JOIN
          , LD 0 0
          , LD 0 0
          , LDC 1
          , SUB
          , LDF 14
          , AP 1
          , MUL
          , JOIN
          , RTN
          , LD 0 0
          , LD 0 0
          , MUL
          , RTN
          ]

foldlLDF :: SECDProgram
foldlLDF = [ DUM 9
           , LDC 1
           , LDC 1103515245
           , LDC 12345
           , LDF 29
           , LDF 46
           , LDF 52
           , LDF 57
           , LDF 61
           , LDF 62
           , LDF 13
           , RAP 9
           , RTN
           , LDF 57
           , LDC 0
           , LDC 1
           , LDC 2
           , LDC 3
           , LDC 4
           , LDC 5
           , LDC 0
           , CONS
           , CONS
           , CONS
           , CONS
           , CONS
           , LDF 46
           , AP 3
           , RTN
           , LD 0 1
           , ATOM
           , SEL 33 35
           , RTN
           , LD 0 2
           , JOIN
           , LD 0 0
           , LD 0 1
           , CDR
           , LD 0 1
           , CAR
           , LD 0 2
           , LD 0 0
           , AP 2
           , LDF 29
           , AP 3
           , JOIN
           , LDF 52
           , LD 0 2
           , LD 0 1
           , LDF 29
           , AP 3
           , RTN
           , LD 0 1
           , LD 0 0
           , LD 1 0
           , AP 2
           , RTN
           , LD 0 0
           , LD 0 1
           , ADD
           , RTN
           , RTN
           , LD 0 0
           , LD 0 0
           , MUL
           , RTN
           ]

randomRecLD :: SECDProgram
randomRecLD = [ DUM 6
              , LDC 1
              , LDC 1103515245
              , LDC 12345
              , LDF 14
              , LDF 47
              , LDF 48
              , LDF 10
              , RAP 6
              , RTN
              , LDC 5
              , LD 0 3
              , AP 1
              , RTN
              , LD 0 0
              , LDC 0
              , CGT
              , SEL 19 45
              , RTN
              , LD 1 0
              , LD 1 1
              , MUL
              , LD 1 2
              , ADD
              , ST 1 0
              , LD 1 0
              , LDC 0
              , CGTE
              , SEL 37 39
              , LDF 47
              , AP 1
              , LD 0 0
              , LDC 1
              , SUB
              , LD 1 3
              , AP 1
              , JOIN
              , LD 1 0
              , JOIN
              , LD 1 0
              , LDC (-1)
              , MUL
              , ST 1 0
              , LD 1 0
              , JOIN
              , LDC 5
              , JOIN
              , RTN
              , LD 0 0
              , LD 0 0
              , MUL
              , RTN
              ]

factTCO :: SECDProgram
factTCO = [ DUM 6
          , LDC 1
          , LDC 1103515245
          , LDC 12345
          , LDF 13 -- LDF POPN
          , LDF 14 -- LDF fact
          , LDF 18 -- LDF fact-helper
          , LDF 10 -- LDF init
          , RAP 6
          , RTN
            -- init:
          , LDC 5
          , LD 0 4
          , TAP 1
            -- POPN:
          , RTN
            -- fact:
          , LD 0 0
          , LDC 1
          , LD 1 5
          , TAP 2
            -- fact-helper:
          , LDC 2
          , LD 0 0
          , CGT
          , TSEL 22 24 -- TSEL branch-0 branch-1
            -- branch-0:
          , LD 0 1
          , RTN
            -- branch-1:
          , LD 0 0
          , LDC 1
          , SUB
          , LD 0 0
          , LD 0 1
          , MUL
          , LD 1 5
          , AP 2
          , RTN
          ]

testMultipleSteps :: SECDProgram -> TestTree
testMultipleSteps code = testCase "Test multiple steps" $ do
  a <- runGCC code Nothing
  (state, closure) <- case a of
        RSuccess [DCons state closure] -> return (state, closure)
        _ -> do assertFailure $ "got this on first step:" ++ show a
                undefined
  return ()
  b <- runGCC code (Just (closure, [state, DCons (DInt 0) (DInt 1)]))
  assertEqual "second step" (RSuccess [DCons (DInt 43) (DInt 2)]) b

-- got from specification
counting_lambda_man = genGCC $ do
  dum  2        -- 2 top-level declarations
  ldc  2        -- declare constant down
  ldf  "step"   -- declare function step
  ldf  "init"   -- init function
  rap  2        -- load declarations into environment and run init
  rtn           -- final return
  label "init"
  ldc  42
  ld   0 1      -- var step
  cons
  rtn           -- return (42, step)
  label "step"
  ld   0 0      -- var s
  ldc  1
  add
  ld   1 0      -- var down
  cons
  rtn           -- return (s+1, down)
