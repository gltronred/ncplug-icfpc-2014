module Helpers where

import GCC.Compiler
import GCCVM
import LambdaDSL

import Test.Tasty
import Test.Tasty.HUnit

success st = RSuccess {r_stack = st}

dlist [] = DInt 0
dlist (x:xs) = DCons (DInt x) $ dlist xs

make_gccvm_test label code expected = testCase label $ do
  res <- runGCC code Nothing
  assertEqual label expected res

compilerTest label lib expected scr = let
  code = compile (if lib then Just stdlib else Nothing) $ script scr
  in make_gccvm_test label code expected



