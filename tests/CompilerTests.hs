module CompilerTests (module CompilerTests) where

import           GCC.Compiler (compile)
import           GCC.Types
import           GCCVM
import           HLDSL
import           LambdaDSL
import qualified LambdaDSL as DSL

import           Helpers

import           Test.Tasty
import           Test.Tasty.HUnit

allCompilerTests :: [TestTree]
allCompilerTests = [ consedResult
                   , prognResult
                   , manyLets
                   , randTest
                   , recRand
                   , letRand
                   , foldTest
                   , mutualRecursive
                   , fact5TCO
                   , set
                   , get
                   , doubleCall
                   ]


mutualRecursive = compilerTest "Mutual recursive functions" False (success $ [dlist [1,0,1,0,1,0]]) $
                  [
                    defn "odd" ["x"]
                    [
                      iff (eq (var "x") (Const 1))
                          (Const 1)
                          (iff (eq (var "x") (Const 0))
                               (Const 0)
                               (call "even" [sub (var "x") (Const 1)]))
                    ],
                    defn "even" ["x"]
                    [
                      iff (eq (Const 0) (var "x"))
                          (Const 1)
                          (iff (eq (Const 1) (var "x"))
                               (Const 0)
                               (call "odd" [sub (var "x") (Const 1)]))
                    ],
                    list [ call "even" [Const 0]
                         , call "even" [Const 1]
                         , call "even" [Const 2]
                         , call "even" [Const 3]
                         , call "odd"  [Const 1]
                         , call "odd"  [Const 2]
                         ]
                  ]

manyLets = compilerTest "Many lets" False (success [DInt 3]) $
           [
             defn "f" ["x"]
             [
               llet ["y"] [cdr $ var "x"]
               [
                 llet ["z"] [cdr $ var "y"]
                 [
                   car $ var "z"
                 ]
               ]
             ],
             call "f" [list $ map Const [1,2,3,4]]
           ]

consedResult = compilerTest "Function result in Cons" False (success [DCons (DInt 2) (DInt 1)]) $
               [
                 App
                   (Lam [Name "x"]
                    (Cons
                     (var "x")
                     (sub (var "x") (Const 1))))
                   [Const 2]
               ]

prognResult = compilerTest "Function result in Progn" False (success [DInt 1]) $
               [
                 App
                   (Lam [Name "x"]
                    (Progn
                     [ var "x"
                     , sub (var "x") (Const 1)
                     ]))
                   [Const 2]
               ]

randTest = compilerTest "Call random" False (success [dlist [1103527590]]) $
           [
             list [ rand
                  ]
           ]

recRand = compilerTest "Call random inside recursive procedure" True (success [DInt 5]) $
          [
            defn "f" ["n"]
            [
              iff (gt (var "n") (Const 0))
                  (Progn
                   [ rand
                   , call "f" [sub (var "n") (Const 1)]])
                  (Const 5)
            ],
            call "f" [Const 5]
          ]

letRand = compilerTest "Call random inside let binding" False (success [DInt 1103527590]) $
          [
            llet ["x"] [list [Const 1, Const 2]]
            [
              llet ["y"] [cdr $ var "x"]
              [
                llet ["z"] [car $ var "y"]
                [
                  rand
                ]
              ]
            ]
          ]

foldTest = compilerTest "Foldl test" True (success [DInt 15]) $
           [
             stdFoldl (lambda ["acc", "x"] $ add (var "acc") (var "x"))
                      (Const 0)
                      (map Const [1..5])
           ]

fact5TCO = compilerTest "Fact(5) with TCO" False (success [DInt 120]) $
           [
             defn "fact" ["n"]
             [
               defn "fact-helper" ["n","acc"]
               [
                 iff (var "n" `lt` Const 2)
                     (var "acc")
                     (call "fact-helper"
                        [ sub (var "n") (Const 1)
                        , mul (var "n") (var "acc") ])
               ],
               call "fact-helper" [var "n", Const 1]
             ],
             call "fact" [Const 5]
           ]

badBinaryNumbers :: [IntValue]
badBinaryNumbers = [0,1,2] ++ map (globalArraySize-) [0,1,2] ++ map ((globalArraySize `Prelude.div` 2) +) [-2..2]

set = testGroup "Set tests" $ flip map badBinaryNumbers $ \i ->
  compilerTest ("Set(" ++ show i ++ ")") False (success [DInt 1]) $
    [
      stdSet (Const i) (Const 1),
      unsafePerformLD 0 $ toInteger $ 6 + i
    ]

get = testGroup "Get tests" $ flip map badBinaryNumbers $ \i ->
  compilerTest ("Get(" ++ show i ++ ")") False (success [DInt 1]) $
    [
      Const 1,
      unsafePerformST 0 (toInteger $ 6 + i),
      stdGet (Const i)
    ]

doubleCall = compilerTest "Double call" False (success [DCons (DInt 1) (DInt 2)]) [
    defn "coward" ["s"] [
      llet ["loc"] [add (Const 2) $ var "s"] [
        (Cons (var "s") (Const 2))
      ]
    ],
    defun "selectDir" ["s"] (\[s] ->
      iff (eq s (Const 1))
          (call "coward" [s])
          (call "coward" [Const 1])
    ),
    defn "step" ["s"] [
      letn ["loc"] [var "s"] (\[loc] ->
        iff (eq loc (c 0))
            (call "selectDir" [Const 2])
            (call "selectDir" [Const 1])            
      )
    ],
    (call "step" [Const 1])
  ]

doubleCallFn = [
    defn "coward" ["s"] [
      llet ["loc"] [add (Const 2) $ var "s"] [
        (Cons (var "s") (Const 2))
      ]
    ],
    defun "selectDir" ["s"] (\[s] ->
      iff (eq s (Const 1))
          (call "coward" [s])
          (call "coward" [Const 1])
    ),
    defn "step" ["s"] [
      letn ["loc"] [var "s"] (\[loc] ->
        iff (eq loc (c 0))
            (call "selectDir" [Const 2])
            (call "selectDir" [Const 1])            
      )
    ],
    (call "step" [Const 1])
  ]

