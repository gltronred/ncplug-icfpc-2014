module AITests (allAITests) where

import GCC.Types
import GCCVM
import LambdaDSL

import Test.Tasty
import Test.Tasty.HUnit

import Helpers

allAITests :: [TestTree]
allAITests = [ saveMap
             , getSavedCell
             , updateSavedCell ]


testMap = list $ map (list . map Const)
          [ [1, 2, 3, 4]
          , [5, 6, 7, 8]
          , [9,10,11,12] ]


saveMap = testGroup "SaveMap tests" $ map saveMapTest [(0,1),(4,5),(5,6),(11,12)]

getSavedCell = testGroup "GetSavedCell tests" $ map getSavedCellTest
               [ ("not on the map", -1,-1,0)
               , ("not on the map", -1, 1,0)
               , ("not on the map",  1,-1,0)
               , ("not on the map",  3, 1,0)
               , ("not on the map",  1, 4,0)
               , ("top-left corner", 0, 0,1)
               , ("right orientation", 1,0, 5)
               , ("right orientation", 0,1, 2)
               , ("internal", 1,2, 7)
               , ("bottom-right corner", 2,3, 12)]

updateSavedCell = testGroup "GetSavedCell tests" $ map updateSavedCellTest
               [ ("not on the map", -1,-1,0)
               , ("not on the map", -1, 1,0)
               , ("not on the map",  1,-1,0)
               , ("not on the map",  3, 1,0)
               , ("not on the map",  1, 4,0)
               , ("top-left corner", 0, 0,1)
               , ("right orientation", 1,0, 1)
               , ("right orientation", 0,1, 1)
               , ("internal", 1,2, 1)
               , ("bottom-right corner", 2,3, 1)]


saveMapTest (i,x) = compilerTest ("SaveMap and get array[" ++ show i ++ "]") True (success [DInt x]) $
                    [
                      call "SaveMap" [testMap],
                      call "GET" [Const $ mapOffset + i]
                    ]

getSavedCellTest (label, i,j, x) = compilerTest ("GetSavedCell that is " ++ label ++ " ("++show i++","++show j++")")
                                   True
                                   (success [DInt x])
                                   [
                                     call "SaveMap" [testMap],
                                     call "GetSavedCell" [Const i,Const j]
                                   ]

updateSavedCellTest (label, i,j, x) = compilerTest ("UpdateSavedCell that is " ++ label ++ " ("++show i++","++show j++")")
                                   True
                                   (success [DInt $ Value x])
                                   [
                                     call "SaveMap" [testMap],
                                     call "UpdateSavedCell" [Const i,Const j, Const x],
                                     call "GetSavedCell" [Const i,Const j]
                                   ]


