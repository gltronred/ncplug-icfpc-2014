import           GCCVM
import           GCC.Compiler (compile)
import           GCC.Assembler
import           GCC.Types

import           LambdaDSL hiding (add, mul, sub, div)
import qualified LambdaDSL as DSL

import           Test.Tasty
import           Test.Tasty.HUnit

import           AITests
import           CompilerTests
import           Helpers
import           VMTests

-- Compiler && VM Tests

gcc_example_1 =
  [ LDC  21
  , LDF  4 --     ; load body
  , AP   1 --        ; call body with ;1 variable in a new frame
  , RTN
  , LD   0 0 --      ; var x
  , LD   0 0 --      ; var x
  , ADD
  , RTN ] :: SECDProgram

gcc_example_2 = genGCC $ do
  dum  2        -- 2 top-level declarations
  ldf  "go"       -- declare function go
  ldf  "to"       -- declare function to
  ldf  "main"     -- main function
  rap  2        -- load declarations into environment and run main
  rtn           -- final return
  label "main"
  ldc  1
  ld   0 0      -- var go
  ap   1        -- call go(1)
  rtn
  label "to"
  ld   0 0      -- var n
  ldc  1
  sub
  ld   1 0      -- var go
  ap   1        -- call go(n-1)
  rtn
  label "go"
  ld   0 0      -- var n
  ldc  1
  add
  ld   1 1      -- var to
  ap   1        -- call to(n+1)
  rtn

lambdaman_going_down = genGCC $ do
  dum  2        -- 2 top-level declarations
  ldc  2        -- declare constant down
  ldf  "step"     -- declare function step 
  ldf  "init"     -- init function
  rap  2        -- load declarations into environment and run init
  rtn           -- final return
  label "init"
  ldc  42
  ld   0 1      -- var step
  cons
  rtn           -- return (42, step)
  label "step"
  ld   0 0      -- var s
  ldc  1
  add
  ld   1 0      -- var down
  cons
  rtn           -- return (s+1, down)

build_3_nats = genGCC $ do
  ldc 1
  ldc 3
  ldf "natural"
  ap 2
  rtn
  label "natural"  -- args: current max
  ld 0 0            -- cur
  ld 0 1            -- max
  cgt               -- exit if cur > max
  sel "exit" "next"
  rtn
  label "next"
  ld 0 0
  ld 0 0
  ldc 1
  add
  ld 0 1
  ldf "natural"
  ap 2
  join
  label "exit"
  join


build_list_len_3 = genGCC $ do
  ldc 0  -- nil result
  ldc 1
  ldc 3
  ldf "natural"
  ap 2
  rtn
  label "natural"  -- args: current max
  ld 0 0            -- cur
  ld 0 1            -- max
  cgt               -- exit if cur > max
  sel "exit" "next"
  rtn
  label "next"
  ld 0 0
  ldf "swap"
  ap 2
  cons
  ld 0 0
  ldc 1
  add
  ld 0 1
  ldf "natural"
  ap 2
  join
  label "exit"
  join
  -- library
  swap

-- swaps two arguments on stack
swap = do
  label "swap"
  ld 0 1
  ld 0 0
  rtn

-- DSL Interpreter tests
map_test = script [
  mapImpl,
  call "Map" [lambda ["x"] (DSL.mul (var "x") (2::IntValue)), list [Const 1, Const 2, Const 3]]
  ]

modifyNth_test = script [
  modifyNth (lambda ["x"] (DSL.mul (var "x") (2::IntValue)) ) (c 1) (list [c 1,c 2,c 3])
  ]

dsl_evaluator_test = testCase "DSL Evaluator test" $ do
  assertEqual "map works in interpreter" (Cons (Const 2) (Cons (Const 4) (Cons (Const 6) (Const 0)))) (evalScript map_test)

compiler_tests = [ make_gccvm_test "!= 42" gcc_example_1 (success [DInt 42])
                 , make_gccvm_test "factorial 5 != 120" (compile Nothing fact5) (success [DInt 120])
                 , make_gccvm_test "3 nats" build_3_nats (success [DInt 3, DInt 2, DInt 1])
                 , make_gccvm_test "list_len_3" build_list_len_3 (success [DCons (DInt 3) (DCons (DInt 2) (DCons (DInt 1) (DInt 0)))])
                 , make_gccvm_test "compiled map" (compile Nothing map_test) (success [DCons (DInt 2) (DCons (DInt 4) (DCons (DInt 6) (DInt 0)))])
                 ] ++ allCompilerTests

interpreter_tests = [ dsl_evaluator_test
                    ]

dsl_stdlib_tests = [ make_gccvm_test "modifyNth" (compile (Just stdlib) modifyNth_test)
                       (success [DCons (DInt 1) (DCons (DInt 4) (DCons (DInt 3) (DInt 0)))])]

main = defaultMain $ testGroup "Tests"
       [ testGroup "Compiler Tests" compiler_tests
       , testGroup "Interpreter Tests" interpreter_tests
       , testGroup "VM Tests" allVMTests
       , testGroup "DSL Stdlib Tests" dsl_stdlib_tests
       , testGroup "AI Helper Tests" allAITests
       ]
