{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Monad
import System.Console.ANSI

import GE.Types
import GE.Sim
import GE.Compat

import LambdaDSL hiding (not)
import StrayAI
import SemiStupidAI
import HungryAI
import HLDSL hiding (isWall)


-- simargs = SimArgs (aiCompiledFromDSL lookAhead3AI) [keyboardControlledAI, randomAI, randomAI]
simargs = SimArgs (aiCompiledFromDSL rayAI) [randomAI, randomAI, randomAI]

main :: IO ()
main = do
  clearScreen
  w <- loadFromFile "maps/basic.map"
  sim simargs w $ \reason ss -> do
    case reason of
      Loose _ -> do
        putStrLn "You loose :("

      Win -> do
        putStrLn "You Win!"

      Tick -> do
        hideCursor
        setCursorPosition 0 0
        let w' = world ss
        let s = (drawStatus w') ++ (drawWorld w')
        let nlines = length $ lines s
        putStr s
        putStrLn "\nNote: for keyboard control use ASDW keys"
        showCursor

        return ()

test_worldToDSL = do
  w <- loadFromFile "maps/basic.map"
  let
    w_ast = worldToDSL w
    (width,height) = levelSize w

    run :: DSLast -> DSLast
    run ast = fst $ runScript (Progn [ast])
  do
    putStrLn "Converting them map to LambdaDSL format and back"
    forM_ [0..(height-1)] $ \y -> do
      forM_ [0..(width-1)] $ \x -> do
        let
          c = run (getMapCell (Cons (Const x) (Const y)) (getMap w_ast))
        do
          if c == (Const 0) then do
            when (not $ isWall (getCell (x,y) w)) $ do
              fail $ "error at " ++ (show (x,y))
            putStr "#"
          else putStr " "
      putStr "\n"
    putStrLn "OK"
