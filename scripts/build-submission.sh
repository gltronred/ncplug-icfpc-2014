#!/bin/bash

if [[ $# < 2 || $# > 5 ]]
then
    echo "Usage: $0 <solution-file> <ghost0-file> [<ghost1-file> ...]"
    echo "Allowed up to four ghost files"
    exit 1
fi

SOL=$1
NAME=ncplug-icfpc-$(date +%Y-%m-%d-%H-%M).tar.gz
URL=ftp/$NAME

rm -rf /tmp/ncplug-submission
mkdir /tmp/ncplug-submission
mkdir /tmp/ncplug-submission/solution
tr -d "()" < $SOL > /tmp/ncplug-submission/solution/lambdaman.gcc
shift 1
i=0
while [[ $# > 0 ]]
do
    cp $1 /tmp/ncplug-submission/solution/ghost$i.ghc
    i=$(($i + 1))
    shift 1
done
git clone git@bitbucket.org:gltronred/ncplug-icfpc-2014.git /tmp/ncplug-submission/code/
cd /tmp/ncplug-submission
cp -R code/maps ./maps
tar cvzf $NAME code/ maps/ solution/

curl -T $NAME ftp://ncplug:OIk81OYK@fosslabs.ru/
SHA1=$(sha1sum $NAME | sed "s/ .*//g;")

echo "================================================================================================"
echo "Submit $NAME to judges"
echo "================================================================================================"
echo "Team name:                  NCPLUG"
echo "Team contact email address: mz@fosslabs.ru"
echo "Programming language(s):    Haskell"
echo "Team homepage URL:          http://ncplug.fosslabs.ru"
echo "Names of participants:"
while read line
do
    echo "* $line"
done < code/PARTICIPANTS
echo "URL of submission:          http://ncplug.fosslabs.ru/$URL"
echo "SHA1 checksum:              $SHA1"
echo "Private identifier:         MFhwghPS"
echo "================================================================================================"

