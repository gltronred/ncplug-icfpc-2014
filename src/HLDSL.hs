module HLDSL where

import Data.Map.Strict (Map, (!), lookup, insert, union, fromList, empty)

import LambdaDSL hiding (not, and, or)
import qualified LambdaDSL as DSL


{-
The Ghosts' and Lambda-Man's direction is an enumeration:
  * 0: up;
  * 1: right;
  * 2: down;
  * 3: left.
-}

dirUp    = Const 0
dirRight = Const 1
dirDown  = Const 2
dirLeft  = Const 3

{-
The state of the world is encoded as follows:

A 4-tuple consisting of

1. The map;
2. the status of Lambda-Man;
3. the status of all the ghosts;
4. the status of fruit at the fruit location.
-}

{-
The map is encoded as a list of lists (row-major) representing the 2-d
grid. An enumeration represents the contents of each grid cell:

  * 0: Wall (`#`)
  * 1: Empty (`<space>`)
  * 2: Pill 
  * 3: Power pill
  * 4: Fruit location
  * 5: Lambda-Man starting position
  * 6: Ghost starting position
-}

{-
The Lambda-Man status is a 5-tuple consisting of:
  1. Lambda-Man's vitality;
  2. Lambda-Man's current location, as an (x,y) pair;
  3. Lambda-Man's current direction;
  4. Lambda-Man's remaining number of lives;
  5. Lambda-Man's current score.

Lambda-Man's vitality is a number which is a countdown to the expiry of
the active power pill, if any. It is 0 when no power pill is active.
  * 0: standard mode;
  * n > 0: power pill mode: the number of game ticks remaining while the
           power pill will will be active
-}

{-
The status of all the ghosts is a list with the status for each ghost.
The list is in the order of the ghost number, so each ghost always appears
in the same location in the list.

The status for each ghost is a 3-tuple consisting of
  1. the ghost's vitality
  2. the ghost's current location, as an (x,y) pair
  3. the ghost's current direction

The Ghosts' vitality is an enumeration:
  * 0: standard;
  * 1: fright mode;
  * 2: invisible.
-}

{-
The status of the fruit is a number which is a countdown to the expiry of
the current fruit, if any.
  * 0: no fruit present;
  * n > 0: fruit present: the number of game ticks remaining while the
           fruit will will be present.
-}

{-
Lambda-Man's move is a direction as encoded above.
-}

{-
parseWorld :: DSLast -> DSLworld
parseWorld arg = DSLworld wm lman gs fr
    where
        m = car arg
        man = car $ cdr arg
        gsts = car $ cdr $ cdr arg
        fruit = car $ cdr $ cdr $ cdr arg

        wm = parseMap m
        lman = parseMan man
        gs = parseGhosts gsts
        fr = parseFruit fruit

parseMan :: DSLast -> LambdaMan
parseMan m = LambdaMan vtlt lctn dctn livs scor
    where
        vit = car m
        loc = car $ cdr m
        dir = car $ cdr $ cdr m
        lvs = car $ cdr $ cdr $ cdr m
        scr = car $ cdr $ cdr $ cdr $ cdr m

        vtlt = case vit of Const v -> Vitality v
        lctn = let x = car loc
                   y = cdr loc
               in case (x, y) of (Const i, Const j) -> (i, j)
        dctn = case dir of Const d -> fromIntValue d
        livs = case lvs of Const l -> l
        scor = case scr of Const s -> s

parseMap :: DSLast -> WorldMap
parseMap = undefined

parseGhosts :: DSLast -> [Ghost]
parseGhosts = undefined

parseFruit :: DSLast -> Fruit
parseFruit f = case f of Const v -> Fruit v
-}

extractWorld :: String -> DSLast
extractWorld varName = var varName

getMap, getMan, getGhosts, getFruit :: DSLast -> DSLast
getMap    = car
getMan    = car . cdr
getGhosts = car . cdr . cdr
getFruit  = car . cdr . cdr . cdr


getManVitality, getManLoc, getManDir, getManLives, getManScore :: DSLast -> DSLast
getManVitality = car
getManLoc      = car . cdr
getManDir      = car . cdr . cdr
getManLives    = car . cdr . cdr . cdr
getManScore    = car . cdr . cdr . cdr . cdr


getLocX = car
getLocY = cdr

locUp    loc = Cons (getLocX loc)                      (sub (getLocY loc) (1::IntValue))
locRight loc = Cons (add (getLocX loc) (1::IntValue))  (getLocY loc)
locDown  loc = Cons (getLocX loc)                      (add (getLocY loc) (1::IntValue))
locLeft  loc = Cons (sub (getLocX loc) (1::IntValue))  (getLocY loc)

eqLoc :: DSLast -> DSLast -> DSLast
eqLoc l1 l2 = (eq (getLocX l1) (getLocX l2)) `DSL.and` (eq (getLocY l1) (getLocY l2))


getMapCell :: DSLast -> DSLast -> DSLast
getMapCell loc m = let row = nth (getLocY loc) m
                   in iff (atom row) (Const 0) (nth (getLocX loc) row)

i_wall,i_empty,i_pill,i_power_pill,i_fruit :: IntValue
i_wall = (0::IntValue)
i_empty = (1::IntValue)
i_pill = (2::IntValue)
i_power_pill = (3::IntValue)
i_fruit = (4::IntValue)

isWall, isEmpty, isPill, isPowerPill, isFruitLoc :: DSLast -> DSLast
isWall cell = iff (eq cell i_wall) (Const 1) nil
isEmpty cell = iff (eq cell i_empty) (Const 1) nil
isPill cell = iff (eq cell i_pill) (Const 1) nil
isPowerPill cell = iff (eq cell i_power_pill ) (Const 1) nil
isFruitLoc cell = iff (eq cell i_fruit) (Const 1) nil


getGhostVitality = car
getGhostLoc = car . cdr
getGhostDir = car . cdr . cdr

isGhost :: DSLast -> DSLast -> DSLast
isGhost loc gsts = call "Any" [
                      lambda ["g"] (eqLoc (getGhostLoc $ var "g") loc),
                      gsts
                    ]


sampleMap = list [
    list [Const 0, Const 0, Const 0, Const 0],
    list [Const 0, Const 1, Const 2, Const 0],
    list [Const 0, Const 3, Const 1, Const 0],
    list [Const 0, Const 0, Const 0, Const 0]
  ]
