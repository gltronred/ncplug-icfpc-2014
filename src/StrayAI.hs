module StrayAI where

import LambdaDSL hiding (not, and, or)
import qualified LambdaDSL as DSL
import HLDSL
import GCC.Compiler


-- Stray biorobot AI :))) --
----------------------------

strayAI = script [
        defn "step" ["s", "world"] [
            llet ["w"] [extractWorld "world"] [
                llet ["map", "man"] [getMap $ var "w", getMan $ var "w"] [
                    llet ["loc"] [getManLoc $ var "man"] [
                        llet ["up", "right", "down", "left"] [locUp $ var "loc", locRight $ var "loc", locDown $ var "loc", locLeft $ var "loc"] [
                            iff (isWall $ getMapCell (var "up") (var "map"))
                                (iff (isWall $ getMapCell (var "right") (var "map")) 
                                    (iff (isWall $ getMapCell (var "down") (var "map"))
                                        (iff (isWall $ getMapCell (var "left") (var "map"))
                                            (Cons (var "s") dirUp)
                                            (Cons (var "s") dirLeft))
                                        (Cons (var "s") dirDown))
                                    (Cons (var "s") dirRight))
                                (Cons (var "s") dirUp)
                        ]
                    ]
                ]
            ]
        ],
        Cons (Const 42) (var "step")
    ]


strayCoward = script [
        defn "step" ["s", "world"] [
            llet ["w"] [extractWorld "world"] [
                llet ["map", "man", "gsts"] [getMap $ var "w", getMan $ var "w", getGhosts $ var "w"] [
                    llet ["loc"] [getManLoc $ var "man"] [
                        llet ["up", "right", "down", "left"] [locUp $ var "loc", locRight $ var "loc", locDown $ var "loc", locLeft $ var "loc"] [
                            iff ((isWall $ getMapCell (var "up") (var "map")) `DSL.or` (isGhost (var "up") (var "gsts")))
                                (iff ((isWall $ getMapCell (var "right") (var "map")) `DSL.or` (isGhost (var "right") (var "gsts")))
                                    (iff ((isWall $ getMapCell (var "down") (var "map")) `DSL.or` (isGhost (var "down") (var "gsts")))
                                        (iff ((isWall $ getMapCell (var "left") (var "map")) `DSL.or` (isGhost (var "left") (var "gsts")))
                                            (Cons (var "s") dirUp)
                                            (Cons (var "s") dirLeft))
                                        (Cons (var "s") dirDown))
                                    (Cons (var "s") dirRight))
                                (Cons (var "s") dirUp)
                        ]
                    ]
                ]
            ]
        ],
        Cons (Const 42) (var "step")
    ]

