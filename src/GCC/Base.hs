module GCC.Base where

import           GCC.Types
import           LambdaDSL

import           Control.Monad.State
import           Data.List (foldl')
import qualified Data.Map.Strict as M

import           Debug.Trace

type Code = [SECDLine]

data Function = Function { fnName :: Name
                         , fnLabel :: Label
                         , fnArgNumber :: Int
                         , fnIsLambda :: Bool
                         , fnCode :: Code }
                deriving (Show)

data SECDEnv = SECDEnv { secdEnv :: [[Name]]
                       , secdFns :: M.Map String Function
                       , secdLabel :: Int }
               deriving (Show)

getNewId :: String -> State SECDEnv String
getNewId prefix = do
  env <- get
  let curId = secdLabel env
  put $ env { secdLabel = curId + 1 }
  return $ prefix ++ "-" ++ show curId

isDefn :: DSLast -> Bool
isDefn (Defn {}) = True
isDefn _ = False

stackSize :: DSLast -> Integer
stackSize (Defn {}) = 0
stackSize (Ld {}) = 0
stackSize (St {}) = (-1)
stackSize (Call (Name "GET") _) = 0
stackSize (Call (Name "SET") _) = (-1)
stackSize (Call (Name "SaveMap") _) = 0
stackSize (Call (Name "UpdateSavedCell") _) = 0
stackSize (PopN n) = -n
stackSize _ = 1

cmd :: SECDCommand Label -> SECDLine
cmd c = SECDLine c Nothing

label :: String -> SECDLine
label = SECDLabel

recCall :: Int -> Label -> SECDLine
recCall = SECDRecCall

resolveLabels :: Code -> SECDProgram
resolveLabels lines = let
  labels = snd $ foldl' collectLabel (0,[]) lines
  isCommand (SECDLabel _) = False
  isCommand (SECDLine {}) = True
  in map (replaceLabel labels) $ filter isCommand lines

collectLabel :: (Int,[(Label,Address)]) -> SECDLine -> (Int,[(Label,Address)])
collectLabel (i,labels) (SECDLabel l) = (i, (Label l,Address i):labels)
collectLabel (i,labels) (SECDLine cmd comment) = (i+1, labels)

replaceLabel :: [(Label,Address)] -> SECDLine -> SECDCommand Address
replaceLabel labels (SECDLine (SEL l1 l2) _) = let
  Just a1 = lookup l1 labels
  Just a2 = lookup l2 labels
  in SEL a1 a2
replaceLabel labels (SECDLine (LDF l) _) = let
  Just a = lookup l labels
  in LDF a
replaceLabel labels (SECDLine (TSEL l1 l2) _) = let
  Just a1 = lookup l1 labels
  Just a2 = lookup l2 labels
  in TSEL a1 a2
replaceLabel label (SECDLine (LDC x) _) = LDC x
replaceLabel label (SECDLine (LD x y) _) = LD x y
replaceLabel label (SECDLine ADD _) = ADD
replaceLabel label (SECDLine SUB _) = SUB
replaceLabel label (SECDLine MUL _) = MUL
replaceLabel label (SECDLine DIV _) = DIV
replaceLabel label (SECDLine CEQ _) = CEQ
replaceLabel label (SECDLine CGT _) = CGT
replaceLabel label (SECDLine CGTE _) = CGTE
replaceLabel label (SECDLine ATOM _) = ATOM
replaceLabel label (SECDLine CONS _) = CONS
replaceLabel label (SECDLine CAR _) = CAR
replaceLabel label (SECDLine CDR _) = CDR
replaceLabel label (SECDLine JOIN _) = JOIN
replaceLabel label (SECDLine (AP x) _) = AP x
replaceLabel label (SECDLine RTN _) = RTN
replaceLabel label (SECDLine (DUM x) _) = DUM x
replaceLabel label (SECDLine (RAP x) _) = RAP x
replaceLabel label (SECDLine STOP _) = STOP
replaceLabel label (SECDLine (TAP x) _) = TAP x
replaceLabel label (SECDLine (TRAP x) _) = TRAP x
replaceLabel label (SECDLine (ST x y) _) = ST x y
replaceLabel label (SECDLine DBUG _) = DBUG
replaceLabel label (SECDLine BRK _) = BRK

resolveRecCalls :: M.Map String Function -> Code -> Code
resolveRecCalls fns = map (replaceRecCall fns)

replaceRecCall :: M.Map String Function -> SECDLine -> SECDLine
replaceRecCall fns line@(SECDLabel _) = line
replaceRecCall fns line@(SECDLine {}) = line
replaceRecCall fns line@(SECDGetSet op idx) = let
  -- TODO: FIX: Why 2??!
  shift = toInteger $ 2 + M.size (M.filter (Prelude.not . fnIsLambda) fns)
  in cmd $ op 1 $ shift + idx
replaceRecCall fns (SECDRecCall top (Label n)) = let
  frame = toInteger top
  fns' = M.filter (Prelude.not . fnIsLambda) fns
  var = toInteger $ M.findIndex n fns'
  in cmd $ LD frame $ var + 3  -- 3 instructions before function list -- why 1??!

