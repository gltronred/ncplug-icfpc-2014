module GCC.Assembler where

import Prelude hiding(join)

import GCC.Types
import GCC.Base (Code(..),resolveLabels)

import Control.Monad.State
import Control.Monad.Writer

type AsmGCC = StateT Int (Writer [SECDLine])

genGCC :: AsmGCC () -> SECDProgram
genGCC code = resolveLabels $ genGCC' code

genGCC' :: AsmGCC () -> Code
genGCC' code = execWriter (evalStateT code 0)

printGCC code = do mapM_ print code

gen :: [SECDLine] -> AsmGCC ()
gen = lift . tell

ldc :: Value -> AsmGCC ()
ldc x = gen $ [cmd $ LDC x]
ld n i = gen $ [cmd $ LD n i]
add = gen $ [cmd $ ADD]
sub = gen $ [cmd $ SUB]
mul = gen $ [cmd $ MUL]
div = gen $ [cmd $ DIV]
ceq = gen $ [cmd $ CEQ]
cgt = gen $ [cmd $ CGT]
cgte = gen $ [cmd $ CGTE]
atom = gen $ [cmd $ ATOM]
cons = gen $ [cmd $ CONS]
car = gen $ [cmd $ CAR]
cdr = gen $ [cmd $ CDR]
sel b1 b2 = gen $ [cmd $ SEL (Label b1) (Label b2)]
join = gen $ [cmd $ JOIN]
ldf addr = gen $ [cmd $ LDF $ Label addr]
ap n = gen $ [cmd $ AP n]
rtn = gen $ [cmd $ RTN]
dum n = gen $ [cmd $ DUM n]
rap n = gen $ [cmd $ RAP n]
stop = gen $ [cmd $ STOP]
tsel b1 b2 = gen $ [cmd $ TSEL b1 b2]
tap n = gen $ [cmd $ TAP n]
trap n = gen $ [cmd $ TRAP n]
st n i = gen $ [cmd $ ST n i]
dbug = gen $ [cmd $ DBUG]
brk = gen $ [cmd $ BRK]

label lb = gen [SECDLabel lb]

cmd c = SECDLine c Nothing

getInd idx = gen [SECDGetSet LD idx]
setInd idx = gen [SECDGetSet ST idx]

