{-# LANGUAGE GeneralizedNewtypeDeriving, TypeSynonymInstances #-}
module GCC.Types where

import Data.Maybe (maybe)

type IntValue = Int
newtype Address = Address Int deriving (Eq, Enum)
newtype Label = Label String deriving (Eq)
newtype Value = Value IntValue deriving (Eq, Num, Enum, Ord, Integral, Real)

instance Show Address where
  show (Address addr) = show addr
instance Show Label where
  show (Label l) = l
instance Num Address where
  fromInteger n = Address $ fromIntegral n
instance Show Value where
  show (Value v) = show v

data SECDCommand addr = LDC Value
                      | LD Integer Integer
                      | ADD
                      | SUB
                      | MUL
                      | DIV
                      | CEQ
                      | CGT
                      | CGTE
                      | ATOM
                      | CONS
                      | CAR
                      | CDR
                      | SEL addr addr
                      | JOIN
                      | LDF addr
                      | AP Integer
                      | RTN
                      | DUM Integer
                      | RAP Integer
                      | STOP
                      | TSEL addr addr
                      | TAP Integer
                      | TRAP Integer
                      | ST Integer Integer
                      | DBUG
                      | BRK
                      deriving (Eq, Show)

data Tag = TAG_INT
         | TAG_CONS
         | TAG_CLOSURE
         deriving (Eq, Show)

data Fault = TagMismatch
           | ControlMismatch
           | FrameMismatch
           | UnknownFail
           deriving (Eq, Show)

data SECDLine = SECDLabel String
              | SECDRecCall Int Label
              | SECDGetSet (Integer -> Integer -> SECDCommand Label) Integer
              | SECDLine { secdCmd :: SECDCommand Label
                         , secdComment :: Maybe String }
              deriving ()

instance Show SECDLine where
  show (SECDLabel label) = label ++ ":"
  show (SECDRecCall frame label) = "  LDREC " ++ show frame ++ " " ++ show label
  show (SECDGetSet op idx) = "  GETSET[" ++ show idx ++ "]"
  show (SECDLine cmd comment) = let
    c = "  " ++ show cmd
    n = length c
    in c ++ maybe "" (replicate (20 - n) ' ' ++) comment

type SECDProgram = [SECDCommand Address]

globalArraySize :: Int
globalArraySize = 15 -- 131071


