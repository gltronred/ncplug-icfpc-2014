module GCC.Compiler (compile
                    ,compileSymb
                    ,resolveLabels
                    ,compileToFile
                    ,compileToFileSymb
                    ) where

import           GCC.Base
import           GCC.Builtins
import           GCC.Types
import           LambdaDSL hiding (not, and, or)

import           Control.Monad.State
import           Data.List (elemIndex, find, foldl', intersperse)
import qualified Data.Map.Strict as M
import           Data.Maybe

compileToFile :: FilePath -> Maybe [DSLast] -> DSLast -> IO ()
compileToFile file mlib = writeFile file . concat . intersperse "\n" .
                          map show . compile mlib

compileToFileSymb :: FilePath -> Maybe [DSLast] -> DSLast -> IO ()
compileToFileSymb file mlib = writeFile file . concat . intersperse
                              "\n" . map show . compileSymb mlib

compile :: Maybe [DSLast] -> DSLast -> SECDProgram
compile mstdlib ast = resolveLabels $ compileSymb mstdlib ast

compileSymb :: Maybe [DSLast] -> DSLast -> Code
compileSymb mstdlib  ast = let
  bins = binDescription
  ast' = maybe ast (Progn . (ast:)) mstdlib
  (code,SECDEnv { secdFns = fns }) = runState (ld ast') (SECDEnv [] bins 0)
  in resolveRecCalls fns $ initCode fns ++ [label "init"] ++ code ++ finalCode fns

ld ast = do
  code <- compile' ast
  builtins <- binCode
  return $ code ++ builtins

initCode fns = let
  defns = M.filter (not . fnIsLambda) fns
  in [ cmd $ DUM $ toInteger $ 4 + M.size defns + globalArraySize
     , cmd $ LDC 1
     , cmd $ LDC 1103515245
     , cmd $ LDC 12345] ++
     map (cmd . LDF . fnLabel) (M.elems defns) ++
     replicate (globalArraySize+1) (cmd $ LDC 0) ++
     [ cmd $ LDF $ Label "init"
     , cmd $ RAP $ toInteger $ 4 + M.size defns + globalArraySize
     , cmd $ RTN]

finalCode fns = concatMap fnCode $ M.elems fns

compile' :: DSLast -> State SECDEnv Code
compile' (Lam {}) = error "Naked lambda"
compile' (Var _) = error "Naked var"
compile' (Defn (Name n) args (Progn body)) = compileDefn n False args body
compile' (Defn (Name n) args _) = error $ "Wrong defn " ++ n ++ " body: no progn"
compile' (Progn stmts) = compileSeq True stmts [cmd RTN]
compile' expr = compileExpr expr [cmd RTN]

compileExpr :: DSLast -> Code -> State SECDEnv Code

compileExpr (Lam args (Progn body)) next = do
  fnId <- getNewId "lam"
  _ <- compileDefn fnId True args body
  return $ [cmd $ LDF $ Label fnId] ++ next
compileExpr (Lam args body) next = do
  fnId <- getNewId "lam"
  _ <- compileDefn fnId True args [body]
  return $ [cmd $ LDF $ Label fnId] ++ next

compileExpr (App fn params) next = do
  let n = toInteger $ length params
  -- TODO: check argument count
  -- (-1) means no check
  fn' <- compileExpr fn $ cmd (AP n) : next
  compileSeq False params fn'

compileExpr (Var name@(Name n)) next = do
  SECDEnv { secdFns = fns, secdEnv = vars } <- get
  let mvar = lookupSECDVar name vars
  case mvar of
    Just (frame, var) -> return $ [cmd $ LD frame var] ++ next
    Nothing -> let mf = M.lookup n fns
               in case mf of
                 Just f -> return $ [cmd $ LDF $ fnLabel f] ++ next
                 Nothing -> error $ "Cannot find symbol " ++ show name

compileExpr (Defn (Name n) args (Progn body)) next = compileDefn n False args body >> return next
compileExpr (Defn (Name n) args body) next = error $ "Wrong defn "++n++" body: no progn"

-- NOTE: do not forget to add internal commands to compileExprTCO
compileExpr (Call name@(Name op) [a,b]) next | op`elem`["Add","Sub","Mul","Div", "Eq","Gt","Ge"] = do
  let Just op' = builtin op
  b' <- compileExpr b $ (cmd op') : next
  compileExpr a b'
compileExpr (Call name@(Name "Lt") [a,b]) next = do
  a' <- compileExpr a $ (cmd CGT) : next
  compileExpr b a'
compileExpr (Call name@(Name "Le") [a,b]) next = do
  a' <- compileExpr a $ (cmd CGTE) : next
  compileExpr b a'
compileExpr (Call name@(Name op) [a]) next | op`elem`["Atom","Car","Cdr"] = do
  let Just op' = builtin op
  compileExpr a $ (cmd op') : next
compileExpr (Call name@(Name "RAND") []) next = binRand next
compileExpr (Call name@(Name fn) args) next = do
  SECDEnv { secdFns = fns, secdEnv = vars } <- get
  let mvar = lookupSECDVar name vars
      argNumber = length args
      top = length vars
  case mvar of
    Nothing -> let maddr = M.lookup fn fns
               in case maddr of
                 Nothing -> error $ "Wrong fn: " ++ fn ++ " " ++ show args
                 Just (Function _ addr argN isLam _) ->
                    if argN == (-1) || argN == argNumber     -- (-1) means no check
                        then if isLam
                             then compileSeq False args $ [cmd $ LDF addr, cmd $ AP $ toInteger argNumber] ++ next
                             else compileSeq False args $ [recCall top addr, cmd $ AP $ toInteger argNumber] ++ next
                        else error $ "Wrong number of arg: " ++ fn ++ " " ++ show args ++ ", need " ++ show argN
    Just (frame, var) -> compileSeq False args $ [cmd $ LD frame var, cmd $ AP $ toInteger argNumber] ++ next

compileExpr (Cons l r) next = do
  r' <- compileExpr r $ (cmd CONS) : next
  compileExpr l r'

compileExpr (Progn stmts) next = compileSeq True stmts next

compileExpr (Const x) next = return $ [cmd $ LDC $ Value x] ++ next

compileExpr (If s t f) next = do
  tId <- getNewId "branch"
  fId <- getNewId "branch"
  s' <- compileExpr s $ cmd (SEL (Label tId) (Label fId)) : next
  t' <- compileExpr t [cmd JOIN]
  f' <- compileExpr f [cmd JOIN]
  return $ s' ++ [label tId] ++ t' ++ [label fId] ++ f'

compileExpr Dbg next = do
  return $ cmd BRK : next

compileExpr (Ld f v) next = return $ cmd (LD f v) : next
compileExpr (St f v) next = return $ cmd (ST f v) : next
compileExpr (PopN n) next = return $ map cmd [LDF $ Label "POPN", AP n] ++ next

compileDefn :: String -> Bool -> [Name] -> [DSLast] -> State SECDEnv Code
compileDefn name isLam args body = let
  n = Name name
  lbl = Label name
  visible env
    | isLam     = args : secdEnv env
    | otherwise = [args]
  in do
    env <- get
    let mfn = M.lookup name $ secdFns env
    let argN = length args
    case mfn of
      Just fn -> if null $ fnCode fn
                 then do
                   put $ env { secdEnv = visible env }
                   body' <- compileSeq True body [cmd RTN]
                   env' <- get
                   put $ env' { secdEnv = secdEnv env
                              , secdFns = M.insert name (fn { fnCode = [label name] ++ body' }) $ secdFns env' }
                   return []
                 else return []
      Nothing -> do
        put $ env { secdEnv = visible env
                  , secdFns = M.insert name (Function n lbl argN isLam []) $ secdFns env }
        body' <- compileSeq True body [cmd RTN]
        let code = label name : body'
        env' <- get
        put $ env' { secdEnv = secdEnv env
                   , secdFns = M.insert name (Function n lbl argN isLam code) $ secdFns env' }
        return []

compileSeq :: Bool -> [DSLast] -> Code -> State SECDEnv Code
compileSeq clearStack stmts next = let
  defns = filter isDefn stmts
  other = filter (not . isDefn) stmts
  totalStack = sum $ map stackSize stmts
  in do
    env <- get
    put $ env { secdFns = M.union (secdFns env) $ M.fromList $ map (\(Defn name@(Name n) args _) -> (n, Function name (Label n) (length args) False [])) defns }
    mapM_ compile' defns
    compileSeqNoDefn (if clearStack then totalStack - 1 else 0) other next

compileSeqNoDefn :: Integer -> [DSLast] -> Code -> State SECDEnv Code
compileSeqNoDefn 0 [] next = return next
compileSeqNoDefn _ [] next = error "Want to clear stack, but no results here"
compileSeqNoDefn count [final@(If s t f)]      next@[SECDLine RTN _] = compileExprTCO final next
compileSeqNoDefn count [final@(Call f params)] next@[SECDLine RTN _] = compileExprTCO final next
compileSeqNoDefn count [final@(App fn params)] next@[SECDLine RTN _] = compileExprTCO final next
compileSeqNoDefn count [stmt,final] next = do
  last' <- compileSeqNoDefn 0 [final] next
  compileExpr stmt $
    (if count > 0 -- should we optimize for count == 1 and count == 2?
     then [ cmd $ LDF $ Label "POPN"
          , cmd $ AP count]
     else []) ++
    last'
compileSeqNoDefn count (stmt:stmts) next = do
  stmts' <- compileSeqNoDefn count stmts next
  compileExpr stmt stmts'

compileExprTCO (App fn params) _ = do
  let n = toInteger $ length params
  -- TODO: check argument count
  -- (-1) means no check
  fn' <- compileExpr fn [cmd (TAP n)]
  compileSeq False params fn'

compileExprTCO command@(Call name@(Name fn) args) next
  | fn`elem`["Add","Sub","Mul","Div", "Eq","Gt","Lt", "Atom","Car","Cdr", "RAND"] = compileExpr command next
  | otherwise = do
    SECDEnv { secdFns = fns, secdEnv = vars } <- get
    let mvar = lookupSECDVar name vars
        argNumber = length args
        top = length vars
    case mvar of
      Nothing -> let maddr = M.lookup fn fns
                 in case maddr of
                   Nothing -> error $ "Wrong fn: " ++ fn ++ " " ++ show args
                   Just (Function _ addr argN isLam _) ->
                     if argN == (-1) || argN == argNumber     -- (-1) means no check
                     then if isLam
                          then compileSeq False args $ [cmd $ LDF addr, cmd $ TAP $ toInteger argNumber]
                          else compileSeq False args $ [recCall top addr, cmd $ TAP $ toInteger argNumber]
                     else error $ "Wrong number of arg: " ++ fn ++ " " ++ show args ++ ", need " ++ show argN
      Just (frame, var) -> compileSeq False args $ [cmd $ LD frame var, cmd $ TAP $ toInteger argNumber]

compileExprTCO (If s t f) next = do
  tId <- getNewId "branch"
  fId <- getNewId "branch"
  s' <- compileExpr s [cmd $ TSEL (Label tId) (Label fId)]
  t' <- compileExpr t [cmd RTN]
  f' <- compileExpr f [cmd RTN]
  return $ s' ++ [label tId] ++ t' ++ [label fId] ++ f'

{-
Built-in functions:
* add
* sub
* mul
* div
* eqp
* gtp
* gtep
* atomp
* car
* cdr
-}

builtin :: String -> Maybe (SECDCommand a)
builtin "Add" = Just ADD
builtin "Sub" = Just SUB
builtin "Mul" = Just MUL
builtin "Div" = Just DIV
builtin "Eq"  = Just CEQ
builtin "Gt"  = Just CGT
builtin "Ge"  = Just CGTE
builtin "Atom"= Just ATOM
builtin "Car" = Just CAR
builtin "Cdr" = Just CDR
builtin _ = Nothing

appendRTN = (++ [cmd RTN])

lookupSECDVar :: Name -> [[Name]] -> Maybe (Integer, Integer)
lookupSECDVar _ [] = Nothing
lookupSECDVar name (frame:frames) = let
  idx = fmap toInteger $ elemIndex name frame
  in if isJust idx
     then Just (0, fromJust idx)
     else fmap (\(f,v) -> (f+1,v)) $ lookupSECDVar name frames


{-

This code will be appended to the output of compiled program

-}
binCode :: State SECDEnv Code
binCode = fmap concat $ sequence []


{-

Changing control example

getNewId prefix
  returns new string for label with given prefix

next parameter
  code that should be executed after this example

NOTE: this function do not works in recursive environments,
      because frame depth in them changes with time!

NOTE: you should also change GCC.Compiler.compileExpr
      (Call (Name "...") ...) to actually use this
      Maybe this will change in future

-}

binRand :: Code -> State SECDEnv Code
binRand next = do
  SECDEnv { secdEnv = vars } <- get
  let top = toInteger $ length vars
  tId <- getNewId "br"
  fId <- getNewId "br"
  return $ [cmd $ LD top 0
           ,cmd $ LD top 1
           ,cmd MUL
           ,cmd $ LD top 2
           ,cmd ADD
           ,cmd $ ST top 0
           ,cmd $ LD top 0
           ,cmd $ LDC 0
           ,cmd CGTE
           ,cmd $ SEL (Label tId) (Label fId)] ++
           next ++
           [label tId
           ,cmd $ LD top 0
           ,cmd JOIN
           ,label fId
           ,cmd $ LD top 0
           ,cmd $ LDC (-1)
           ,cmd MUL
           ,cmd $ ST top 0
           ,cmd $ LD top 0
           ,cmd JOIN]




