module GCC.Builtins (binDescription,generateBinaryImpl) where

import           GCC.Assembler
import           GCC.Base hiding (cmd, label)
import           GCC.Types
import           LambdaDSL

import           Control.Monad.State.Lazy
import qualified Data.Map.Strict as M

{-

There are two sort of builtins:
* one that do not change control
* one that change

-}


-- squareImpl :: Code
-- squareImpl = [label "square"
--              ,cmd $ LD 0 0
--              ,cmd $ LD 0 0
--              ,cmd MUL
--              ,cmd RTN]

popImpl :: Code
popImpl = genGCC' $ do
  label "POPN"
  rtn


getImpl :: Code
getImpl = genGCC' $ do
  label "GET"
  generateBinaryImpl "get" 0 (globalArraySize+1) $ getInd

setImpl :: Code
setImpl = genGCC' $ do
  label "SET"
  ld 0 1
  generateBinaryImpl "set" 0 (globalArraySize+1) $ setInd

generateBinaryImpl :: String -> IntValue -> IntValue -> (Integer -> AsmGCC ()) -> AsmGCC ()
generateBinaryImpl ident l r ops = let
  c = (l+r) `Prelude.div` 2 :: IntValue
  in if r <= l
     then do
       ops $ toInteger l
       rtn
     else do
       ldc $ Value c
       ld 0 0
       cgt
       idx <- get
       let b0 = ident ++ show idx
           b1 = ident ++ show (idx+1)
       tsel (Label b0) (Label b1)
       put $ idx+2
       label b0
       generateBinaryImpl ident l c ops
       label b1
       generateBinaryImpl ident (c+1) r ops
--                                                                           Nargs lambda?
binDescription = M.fromList [ ("POPN", Function (Name "POPN") (Label "POPN") (-1) False popImpl)
                            , ("GET",  Function (Name "GET")  (Label "GET")  1    False getImpl)
                            , ("SET",  Function (Name "SET")  (Label "SET")  2    False setImpl)
                            -- , ("square", Function (Name "square") (Label "square") 1 False squareImpl)
                            ]

