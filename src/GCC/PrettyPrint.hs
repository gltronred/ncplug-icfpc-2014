module GCC.PrettyPrint (pretty) where

import GCC.Types

pretty :: Show a => [SECDCommand a] -> String
pretty = concatMap (\c -> show c ++ "\n")


