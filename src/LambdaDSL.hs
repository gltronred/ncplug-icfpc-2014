{-# LANGUAGE GeneralizedNewtypeDeriving, MultiParamTypeClasses, FlexibleInstances #-}
module LambdaDSL (module LambdaDSL, IntValue) where

import           GCC.Types

import           Control.Monad.State.Lazy
import           Data.Map.Strict (Map, (!), lookup, insert, union, fromList, empty)
import           Data.Maybe (fromMaybe, fromJust)
import qualified Prelude as P
import           Prelude hiding (lookup, div, not, and, or)

data Op = Add | Sub | Mul | Div | Eq | Gt | Lt | Ge | Le | Car | Cdr deriving (Show, Enum)
newtype Name = Name { unName :: String } deriving (Eq, Ord)
data DSLast = Lam [Name] DSLast
            | App DSLast [DSLast]
            | Var Name
            | Defn Name [Name] DSLast
            | Call Name [DSLast]  -- call to predefined function
            | Cons DSLast DSLast
            | Progn [DSLast]
            | Const IntValue
            | If DSLast DSLast DSLast
            | Dbg
            | Ld Integer Integer
            | St Integer Integer
            | PopN Integer
    deriving (Show, Eq)

newtype DSLenv = DSLenv (Map Name DSLast)
newtype Builtins = Builtins (Map Name ([DSLast] -> DSLast))

instance Show Name where
    show (Name n) = n


nil = Const 0

lookupVar :: DSLenv -> Name -> DSLast
lookupVar (DSLenv env) name = fromMaybe nil $ lookup name env

callBuiltin :: Builtins -> Name -> [DSLast] -> DSLast
callBuiltin (Builtins fns) name vals = fromMaybe nil $ do
    f <- lookup name fns
    return $ f vals

evalDSL :: Builtins -> DSLast -> State DSLenv DSLast
evalDSL bins c@(Const _) = return c
evalDSL bins l@(Lam _ _) = return l
evalDSL bins (Cons lhs rhs) = do
    lhs1 <- evalDSL bins lhs
    rhs1 <- evalDSL bins rhs
    return $ Cons lhs1 rhs1
evalDSL bins (Var name) = do
    env <- get
    return $ lookupVar env name
evalDSL bins (Defn name args ast) = do
    (DSLenv env) <- get
    put $ DSLenv $ insert name (Lam args ast) env
    return nil
evalDSL bins (Progn stmts) = do
    vals <- mapM (evalDSL bins) stmts
    return $ last vals
evalDSL bins (Call name args) = do
    (DSLenv env) <- get
    vals <- mapM (evalDSL bins) args
    return $ fromMaybe (callBuiltin bins name vals) $ do
        (Lam params body) <- lookup name env
        guard $ (length params) == (length args)
        let env1 = foldl (\m (k,v) -> insert k v m) env $ zip params vals
        return $ evalState (evalDSL bins body) (DSLenv env1)
evalDSL bins (If cond true false) = do
    c <- evalDSL bins cond
    case c of
        (Const 0) -> evalDSL bins false
        _ -> evalDSL bins true
evalDSL bins (App (Lam params body) args) = do
    (DSLenv env) <- get
    vals <- mapM (evalDSL bins) args
    return $ fromMaybe nil $ do
        guard $ (length params) == (length args)
        let env1 = foldl (\m (k,v) -> insert k v m) env $ zip params vals
        return $ evalState (evalDSL bins body) (DSLenv env1)


evalInt :: DSLast -> Maybe IntValue
evalInt ast = case evalState (evalDSL (Builtins empty) ast) (DSLenv empty) of
    Const i -> Just i
    otherwise -> Nothing

evalCons :: DSLast -> Maybe DSLast
evalCons ast = case evalState (evalDSL (Builtins empty) ast) (DSLenv empty) of
    c@(Cons _ _) -> Just c
    otherwise -> Nothing

addImpl, subImpl, mulImpl, divImpl, eqImpl, gtImpl, ltImpl, geImpl, leImpl, carImpl, cdrImpl :: [DSLast] -> DSLast
addImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    return $ Const $ i1 + i2

subImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    return $ Const $ i1 - i2

mulImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    return $ Const $ i1 * i2

divImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    return $ Const $ i1 `P.div` i2

eqImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    let res = if i1 == i2 then 1 else 0
    return $ Const res

gtImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    let res = if i1 > i2 then 1 else 0
    return $ Const res

ltImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    let res = if i1 < i2 then 1 else 0
    return $ Const res

geImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    let res = if i1 >= i2 then 1 else 0
    return $ Const res

leImpl [a,b] = fromMaybe nil $ do
    i1 <- evalInt a
    i2 <- evalInt b
    let res = if i1 <= i2 then 1 else 0
    return $ Const res

carImpl [c] = fromMaybe (error "bb") $ do
    (Cons h _) <- evalCons c
    return h

cdrImpl [c] = fromMaybe (error "aa") $ do
    (Cons _ tl) <- evalCons c
    return tl

atomImpl :: [DSLast] -> DSLast
atomImpl [arg] = fromMaybe (Const 1) $ do
    (Cons _ _) <- evalCons arg
    return nil


operators = fromList $ zip (map (Name . show) [Add .. Cdr]) [addImpl, subImpl, mulImpl, divImpl, eqImpl, gtImpl, ltImpl, geImpl, leImpl, carImpl, cdrImpl]

builtins = fromList [(Name "Atom", atomImpl)]


{-
  MMMM    MMMM    MM    MM  MMMMMM  MMMMM     MMMM    MM    
 MM      MM  MM   MMMM  MM  MMMMMM  MM  MM   MM  MM   MM    
MM      MM    MM  MM MM MM    MM    MMMM    MM    MM  MM    
 MM      MM  MM   MM  MMMM    MM    MM  MM   MM  MM   MM    
  MMMM    MMMM    MM    MM    MM    MM  MM    MMMM    MMMMMM
-}

script :: [DSLast] -> DSLast
script stmts = Progn stmts

defn :: String -> [String] -> [DSLast] -> DSLast
defn name args stmts = Defn (Name name) (map Name args) (Progn stmts)

iff :: DSLast -> DSLast -> DSLast -> DSLast
iff cond true false = If cond true false

var :: String -> DSLast
var name = Var (Name name)

call :: String -> [DSLast] -> DSLast
call name args = Call (Name name) args

lambda :: [String] -> DSLast -> DSLast
lambda args body = Lam (map Name args) body

llet :: [String] -> [DSLast] -> [DSLast] -> DSLast
llet vars vals body = App (lambda vars (Progn body)) vals

-----------------------------------
-- Better LambdaDSL, from HungryAI
-----------------------------------

lett n v f = llet [n] [v] [f (var n)]
lamm ss f = lambda ss (f (map var ss))
letn ns vs f = llet ns vs [f (map var ns)]
defun name ns f = defn name ns [f (map var ns)]

infixr 1 .:
(.:) = Cons
c i = Const i


class ArithComp a b where
    add, sub, mul, div, eq, gt, lt, ge, le :: a -> b -> DSLast

instance ArithComp DSLast DSLast where
    -- add, sub, mul, div, eq, gt, lt :: DSLast -> DSLast -> DSLast
    add lhs rhs = Call (Name $ show Add) [lhs, rhs]
    sub lhs rhs = Call (Name $ show Sub) [lhs, rhs]
    mul lhs rhs = Call (Name $ show Mul) [lhs, rhs]
    div lhs rhs = Call (Name $ show Div) [lhs, rhs]
    eq lhs rhs = Call (Name $ show Eq) [lhs, rhs]
    gt lhs rhs = Call (Name $ show Gt) [lhs, rhs]
    lt lhs rhs = Call (Name $ show Lt) [lhs, rhs]
    ge lhs rhs = Call (Name $ show Ge) [lhs, rhs]
    le lhs rhs = Call (Name $ show Le) [lhs, rhs]


-- with implicit IntValue argument conversion
instance ArithComp DSLast IntValue where
    -- add, sub, mul, div, eq, gt, lt :: DSLast -> IntValue -> DSLast
    add ast int = add ast (Const int)
    sub ast int = sub ast (Const int)
    mul ast int = mul ast (Const int)
    div ast int = div ast (Const int)
    eq ast int = eq ast (Const int)
    gt ast int = gt ast (Const int)
    lt ast int = lt ast (Const int)
    ge ast int = ge ast (Const int)
    le ast int = le ast (Const int)

instance ArithComp IntValue DSLast where
    -- add, sub, mul, div, eq, gt, lt :: IntValue -> DSLast -> DSLast
    add int ast = add (Const int) ast
    sub int ast = sub (Const int) ast
    mul int ast = mul (Const int) ast
    div int ast = div (Const int) ast
    eq int ast = eq (Const int) ast
    gt int ast = gt (Const int) ast
    lt int ast = lt (Const int) ast
    ge int ast = ge (Const int) ast
    le int ast = le (Const int) ast


not :: DSLast -> DSLast
not a = iff (eq a nil) (Const 1) nil

and, or :: DSLast -> DSLast -> DSLast
and a b = iff (eq a nil) nil b
or  a b = iff (eq a nil) b a


car, cdr :: DSLast -> DSLast
car arg = Call (Name $ show Car) [arg]
cdr arg = Call (Name $ show Cdr) [arg]

atom :: DSLast -> DSLast
atom arg = Call (Name "Atom") [arg]

list :: [DSLast] -> DSLast
list = foldr Cons nil

rand :: DSLast
rand = Call (Name "RAND") []


-- stdlib :) --
---------------
{-
MM         MMMMMMMM    BBBBBB  
MM            MM       BB    BB
MM            MM       BB    BB
MM            MM       BBBBBB  
MM            MM       BB    BB
MMMMMMMM      MM       BB    BB
MMMMMMMM   MMMMMMMM    BBBBBB  
-}

nthImpl :: DSLast
nthImpl= defn "Nth" ["nth-ind", "nth-lst"] [
        iff (atom $ var "nth-lst")
            (nil)
            (iff (eq (var "nth-ind") (0::IntValue))
                 (car $ var "nth-lst")
                 (call "Nth" [
                     (sub (var "nth-ind") (1::IntValue)),
                     (cdr $ var "nth-lst")
                 ]))
    ]

modifyNthImpl = defn "ModifyNth" ["f", "n", "lst"] [
  lett "hd" (car lst) $ \hd ->
   lett "tl" (cdr lst) $ \tl ->
     iff (eq n (c 0))
       (Cons (call "f" [hd]) tl)
       (Cons hd (call "ModifyNth" [f, sub n (c 1), tl]))]
 where f = var "f"
       n = var "n"
       lst = var "lst"

anyImpl :: DSLast
anyImpl = defn "Any" ["anyfn", "anylst"] [
        iff (atom $ var "anylst")
            nil
            (iff (eq (call "anyfn" [(car $ var "anylst")]) nil)
                (call "Any" [var "anyfn", cdr $ var "anylst"])
                (Const 1))
    ]

allImpl :: DSLast
allImpl = defn "All" ["allfn", "alllst"] [
        iff (atom $ var "alllst")
            (Const 1)
            (iff (eq (call "allfn" [(car $ var "alllst")]) nil)
                nil
                (call "All" [var "allfn", cdr $ var "alllst"]))
    ]

mapImpl :: DSLast
mapImpl = defn "Map" ["mapfn", "maplst"] [
        iff (atom $ var "maplst")
            nil
            (Cons (call "mapfn" [(car $ var "maplst")])
                  (call "Map" [var "mapfn", cdr $ var "maplst"]))
    ]

appendImpl :: DSLast
appendImpl = defn "Append" ["applst1", "applst2"] [
        iff (atom $ var "applst1")
            (var "applst2")
            (Cons (car $ var "applst1")
                  (call "Append" [cdr $ var "applst1", var "applst2"]))
    ]

accHelperImpl :: DSLast
accHelperImpl = defn "AccHelper" ["acchelp-fn", "acchelp-lst", "acchelp-acc"] [
        iff (atom $ var "acchelp-lst")
            (var "acchelp-acc")
            (call "AccHelper" [
                (var "acchelp-fn"),
                (cdr $ var "acchelp-lst"),
                (call "acchelp-fn" [car $ var "acchelp-lst", var "acchelp-acc"])])
    ]

reverseImpl :: DSLast
reverseImpl = defn "Reverse" ["rev-lst"] [
        call "AccHelper" [
            lambda ["rev-lam-elem", "rev-lam-acc"] (Cons (var "rev-lam-elem") (var "rev-lam-acc")),
            var "rev-lst",
            nil]
    ]

foldlImpl :: DSLast
foldlImpl = defn "Foldl" ["foldl-fun", "foldl-init", "foldl-lst"]
            [
        -- call "AccHelper" [
        --     lambda ["foldl-lam-elem", "foldl-lam-acc"] (call "foldl-fun" [(var "foldl-lam-acc"), (var "foldl-lam-elem")]),
        --     var "foldl-lst",
        --     var "foldl-init"]
              iff (atom $ var "foldl-lst")
                  (var "foldl-init")
                  (call "Foldl" [var "foldl-fun",
                                 call "foldl-fun" [var "foldl-init", car $ var "foldl-lst"],
                                 cdr $ var "foldl-lst"])
            ]


mapOffset = 1

saveMapImpl :: DSLast
saveMapImpl = let
  countLam = lambda ["acc","x"] (add (var "acc") $ Const 1)
  eltLam = lambda ["jdx","elt"]
           (Progn [
               stdSet (var "jdx") (var "elt"),
               add (var "jdx") (Const 1)])
  rowLam = lambda ["idx","row"]
           (call "Foldl" [eltLam, var "idx", var "row"])
  in defn "SaveMap" ["map"]
  [
    call "SET" [Const 0, call "Foldl" [countLam, Const 0, car $ var "map"]],
    call "Foldl" [rowLam, Const mapOffset, var "map"],
    unsafePerformPOPN 1
  ]

getSavedCellImpl :: DSLast
getSavedCellImpl = let
  wall = Const 0
  i = var "i"
  j = var "j"
  addMapOffsetIfNeeded = if mapOffset == 0 then id else add $ Const mapOffset
  in defn "GetSavedCell" ["i","j"]
  [
    lett "width" (call "GET" [Const 0]) $ \width ->
     iff (i `lt` Const 0)
         wall
         (iff (j `lt` Const 0)
              wall
              (iff (j `ge` width)
                   wall
                   -- NOTE: we do not check i > height, because initially array is filled with 0
                   (stdGet $ addMapOffsetIfNeeded $ add j $ mul i width)))
  ]

updateSavedCellImpl :: DSLast
updateSavedCellImpl = let
  wall = Const 0
  i = var "i"
  j = var "j"
  x = var "x"
  addMapOffsetIfNeeded = if mapOffset == 0 then id else add $ Const mapOffset
  nop = unsafePerformPOPN 0
  in defn "UpdateSavedCell" ["i","j", "x"]
  [
    lett "width" (call "GET" [Const 0]) $ \width ->
     iff (i `lt` Const 0)
         nop
         (iff (j `lt` Const 0)
              nop
              (iff (j `ge` width)
                   nop
                   -- NOTE: we do not check i > height, because initially array is filled with 0
                   (stdSet (addMapOffsetIfNeeded $ add j $ mul i width) x)))
  ]


stdlib = [nthImpl, modifyNthImpl, anyImpl, allImpl, mapImpl, appendImpl, accHelperImpl, reverseImpl, foldlImpl, saveMapImpl, getSavedCellImpl, updateSavedCellImpl]
-- stdlib wrappers --
---------------------

nth :: DSLast -> DSLast -> DSLast
nth ind lst = call "Nth" [ind, lst]

modifyNth f ind lst = call "ModifyNth" [f, ind, lst]

stdFoldl :: DSLast -> DSLast -> [DSLast] -> DSLast
stdFoldl fn zero lst = call "Foldl" [fn, zero, list lst]

stdMap :: DSLast -> [DSLast] -> DSLast
stdMap fn lst = call "Map" [fn, list lst]

stdGet i = call "GET" [i]
stdSet i x = call "SET" [i, x]


fact5 = script [
        defn "fact" ["n"] [
            iff (lt (var "n") (2::IntValue)) (Const 1) (mul (var "n") (call "fact" [sub (var "n") (1::IntValue)]))
        ],
        (call "fact" [(Const 5)])
    ]

foldlFact5 = script [
        (call "Foldl" [
            lambda ["acc", "n"] (mul (var "acc") (var "n")),
            Const 1,
            list [Const 2, Const 3, Const 4, Const 5]])
    ]

testRandom = script [
  defn "main" ["n"] [
     iff (lt (var "n") (Const 1)) (Const 0) (Cons rand (call "main" [sub (var "n") (Const 1)]))
     ],
  (call "main" [Const 10])
  ]

{-
*LambdaDSL> let ((Cons st step), env) = runScript exampleAI
*LambdaDSL> evalScriptWithEnv env $ App step [st, nil]
Cons (Const 43) (Const 2)
-}
exampleAI = script [
        defn "init" ["stepfn"] [
            Cons (Const 42) (var "stepfn")
        ],
        defn "step" ["s", "down"] [
            Cons (add (var "s") (1::IntValue)) (var "down")
        ],
        call "init" [
            (lambda ["s", "world"] (call "step" [(var "s"), (Const 2)]))
        ]
    ]


evalScript :: DSLast -> DSLast
evalScript script = evalState (evalDSL (Builtins funcs) libNscript) (DSLenv empty)
    where
        funcs = operators `union` builtins
        libNscript = case script of Progn stmts -> Progn $ stdlib ++ stmts

runScript :: DSLast -> (DSLast, DSLenv)
runScript script = runState (evalDSL (Builtins funcs) libNscript) (DSLenv empty)
    where
        funcs = operators `union` builtins
        libNscript = case script of Progn stmts -> Progn $ stdlib ++ stmts

evalScriptWithEnv :: DSLenv -> DSLast -> DSLast
evalScriptWithEnv env script = evalState (evalDSL (Builtins funcs) libNscript) env
    where
        funcs = operators `union` builtins
        libNscript = case script of Progn stmts -> Progn $ stdlib ++ stmts


{- ---++++===== UNSAFE =====++++--- -}

unsafePerformLD = Ld
unsafePerformST = St
unsafePerformPOPN = PopN

