module GCCVM(ProgResult(..), Datum(..), runGCC, VM(..))
where

import GCC.Types

import Control.Exception
import Control.Monad.Reader
import Control.Monad.Trans
import Data.Array
import Data.IORef

import System.IO.Unsafe
import System.Environment

gccvm_debug = unsafePerformIO $ do
  env <- getEnvironment
  case lookup "GCCVM_DEBUG" env of
    Nothing -> return False
    Just e -> return True

data CmdResult = MachineStop | MachineCont | MachineDbgBreak
data ProgResult = RTimeout { r_stack :: DataStack } | RFault { r_msg :: String } | RSuccess { r_stack :: DataStack } | RDbgBreak { r_stack :: DataStack } deriving (Show,Eq)

data Datum = DInt Value | DCons Datum Datum | DClosure Address Environment deriving (Eq)

instance Show Datum where
    show (DInt v) = show v
    show (DCons a b) = "[" ++ show a ++ "," ++ show b ++ "]"
    show (DClosure a e) = "<" ++ show a ++ ">"

type CommandStack = [SECDCommand Address]
type DataStack = [Datum]
type ControlStack = [ControlFrame]

type EnvList = Int
type Heap = Int
type ProgramCounter = Address

type FrameData = Array Int (IORef Datum)

instance Show d => Show (IORef d) where
  show d = show $ unsafePerformIO $  readIORef d

data Environment = Environment {
  e_dummy :: IORef Bool,
  e_vars :: FrameData,
  e_parent :: Environment
  } | NullEnv deriving (Show,Eq)

data ControlFrame = CStop | CJoin ProgramCounter | CRet ProgramCounter | CFrame Environment deriving Show --  ControlFrame ProgramCounter Environment

data VM = VM {
  controlR :: IORef ProgramCounter,
  envR :: IORef Environment,
  dataStack :: IORef DataStack,
  controlStack :: IORef [ControlFrame]
  }

type GCCVM = ReaderT VM IO

-- Runs VM
-- on first run pass program and Nothing, on subsequent runs pass program and (closure,args) to restore
runGCC :: SECDProgram -> Maybe (Datum, DataStack) -> IO ProgResult
runGCC code mstack = do
  when gccvm_debug $ do
    liftIO $ putStrLn "***runGCC executed***"
  vm <- makeVM
  catch (runReaderT init_and_run vm) $ \(AssertionFailed msg) -> do
    return $ RFault { r_msg = msg}

  where --runCurCmd :: ~Array Program -> Int -> GCCVM ProgResult
        runCurCmd prog 0 = do stopMachine RTimeout
        runCurCmd prog n = do
         c <- readVM controlR
         let cmd = prog ! (case c of (Address a) -> fromIntegral a)
         vm <- ask
         when gccvm_debug $ do
           liftIO $ printVM vm
           liftIO $ print cmd
         res <- run cmd
         case res of
           MachineStop -> stopMachine RSuccess
           MachineCont -> runCurCmd prog (n-1)
           MachineDbgBreak -> stopMachine RDbgBreak
        stopMachine res = do
           s <- readVM dataStack
           return $ res s
        init_and_run = do
          case mstack of
            Just (closure, args) -> do
              dataStack #= const (closure : reverse args)
              run $ TAP (fromIntegral $ length args)
            Nothing -> return undefined
          let program = listArray (0, length code) code
          runCurCmd program 3070000

makeVM = do
  cr <- newIORef $ entryPoint
  er <- newIORef $ NullEnv
  ds <- newIORef []
  cs <- newIORef [CStop]
  return $ VM { controlR = cr,
                envR = er,
                dataStack = ds,
                controlStack = cs
              }

printVM vm = do
  c <- readIORef $ controlR vm
  e <- readIORef $ envR vm
  ds <- readIORef $ dataStack vm
  cs <- readIORef $ controlStack vm
  print $ "ControlR: " ++ show c
  print $ "Data Stack: " ++ show ds
  print $ "EnvR: " ++ show e
  print $ "Control stack: " ++ show cs

entryPoint = Address 0

-- modifying assignment
infix 1  #=
(#=) :: (VM -> IORef a) -> (a -> a) -> GCCVM ()
(#=) field modifier = do
  ref <- asks field
  liftIO $ modifyIORef ref modifier

-- reading
readVM field = do
  ref <- asks field
  liftIO $ readIORef ref

-- stack operations
pop :: (VM -> IORef [a]) -> GCCVM a
pop field = do
  ref <- asks field
  vals <- liftIO $ readIORef ref
  case vals of
    [] -> fault "pop from empty stack"
    (val:rest) -> do
      liftIO $ writeIORef ref rest
      return val

push field val = do
  ref <- asks field
  vals <- liftIO $ readIORef ref
  liftIO $ writeIORef ref (val:vals)

-- frame data operations
makeDummyFrame :: Integer -> Environment -> GCCVM Environment
makeDummyFrame n parent = do
  dum <- liftIO $ newIORef True
  fd <- liftIO $ makeEmptyFrameData (fromIntegral n)
  return $ Environment { e_dummy = dum, e_vars = fd, e_parent = parent }
makeFrame :: [Datum] -> Environment -> GCCVM Environment
makeFrame vals parent = do
  dum <- liftIO $ newIORef False
  fd <- liftIO $ makeFrameData (length vals) vals
  return $ Environment { e_dummy = dum, e_vars = fd, e_parent = parent }

isDummyFrame :: Environment -> GCCVM Bool
isDummyFrame e = liftIO $ readIORef (e_dummy e)

makeFrameData :: Int -> [Datum] -> IO FrameData
makeFrameData n vals = do
    vars <- mapM newIORef $ take (fromIntegral n) vals
    return $ listArray (0, fromIntegral $ n-1) vars
makeEmptyFrameData n = makeFrameData n $ repeat (DInt 0)
frameData fd k = fd ! fromIntegral k
frameSize :: FrameData -> Integer
frameSize fd = fromIntegral $ snd (bounds fd) + 1

run :: SECDCommand Address -> GCCVM CmdResult

--LDC - load constant
--Synopsis: load an immediate literal;
--          push it onto the data stack
--  %s := PUSH(SET_TAG(TAG_INT,$n),%s)
--  %c := %c+1
run (LDC i) = do
  push dataStack (DInt i)
  incrPC

--LD - load from environment
--Synopsis: load a value from the environment;
--          push it onto the data stack
-- Syntax:  LD $n $i
-- Example: LD 0 1
-- Effect:
--   $fp := %e
--   while $n > 0 do            ; follow chain of frames to get n'th frame
--   begin
--     $fp := FRAME_PARENT($fp)
--     $n := $n-1
--   end
--   $v := FRAME_VALUE($fp, $i) ; i'th element of frame
--   %s := PUSH($v,%s)          ; push onto the data stack
--   %c := %c+1
run (LD n i) = do
  fp <- readVM envR
  p <- n_times lookup_parent fp n
  v <- liftIO $ readIORef $ frameData (e_vars p) i
  push dataStack v
  incrPC

run ADD 	= runIntOp (+)
run SUB		= runIntOp (-)
run MUL		= runIntOp (*)
run DIV		= runIntOp (\x y -> if y == 0 then throw (AssertionFailed "Division by zero") else x `div` y)
run CEQ		= runIntOp (\a b -> if a == b then 1 else 0)
run CGT		= runIntOp (\a b -> if a > b then 1 else 0)
run CGTE	= runIntOp (\a b -> if a >= b then 1 else 0)

-- ATOM - test if value is an integer
-- Syntax: ATOM
-- Effect:
--   $x,%s := POP(%s)
--   if TAG($x) == TAG_INT then
--     $y := 1
--   else
--     $y := 0
--   %s := PUSH(SET_TAG(TAG_INT,$y),%s)
--   %c := %c+1
run ATOM = do
  x <- pop dataStack
  let y = case x of
        DInt _ -> 1
        _ -> 0
  push dataStack (DInt y)
  incrPC

-- CONS - allocate a CONS cell
-- Syntax: CONS
-- Effect:
--   $y,%s := POP(%s)
--   $x,%s := POP(%s)
--   $z := ALLOC_CONS($x,$y)
--   %s := PUSH(SET_TAG(TAG_CONS,$z),%s)
--   %c := %c+1
run CONS = do
  y <- pop dataStack
  x <- pop dataStack
  push dataStack (DCons x y)
  incrPC

-- CAR - extract first element from CONS cell
-- Effect:
--   $x,%s := POP(%s)
--   if TAG($x) != TAG_CONS then FAULT(TAG_MISMATCH)
--   $y := CAR($x)
--   %s := PUSH($y,%s)
--   %c := %c+1

run CAR = do
  x <- pop dataStack
  case x of
    DCons a b -> do
      push dataStack a
      incrPC
    _ -> fault "Car from a non-cons"

-- CDR - extract second element from CONS cell
-- Syntax: CDR
-- Effect:
--   $x,%s := POP(%s)
--   if TAG($x) != TAG_CONS then FAULT(TAG_MISMATCH)
--   $y := CDR($x)
--   %s := PUSH($y,%s)
--   %c := %c+1
run CDR = do
  x <- pop dataStack
  case x of
    DCons a b -> do
      push dataStack b
      incrPC
    _ -> fault "CDR from a non-cons"

-- SEL - conditional branch
-- Syntax:  SEL $t $f
-- Example: SEL 335 346  ; absolute instruction addresses
-- Effect:
--   $x,%s := POP(%s)
--   if TAG($x) != TAG_INT then FAULT(TAG_MISMATCH)
--   %d := PUSH(SET_TAG(TAG_JOIN,%c+1),%d)   ; save the return address
--   if $x == 0 then
--     %c := $f
--   else
--     %c := $t

run (SEL t f) = runSEL True t f
run (TSEL t f) = runSEL False t f

-- JOIN - return from branch

-- Synopsis: pop a return address off the control stack, branch to that address
-- Syntax:  JOIN
-- Effect:
--   $x,%d := POP(%d)
--   if TAG($x) != TAG_JOIN then FAULT(CONTROL_MISMATCH)
--   %c := $x
run JOIN = do
  x <- pop controlStack
  case x of
    CJoin xv -> do controlR #= const xv
                   continue
    _ -> fault "Control mismatch, JOIN needs join-frame"


-- LDF - load function
-- Syntax:  LDF $f
-- Example: LDF 634      ; absolute instruction addresses
-- Effect:
--   $x := ALLOC_CLOSURE($f,%e)
--   %s := PUSH(SET_TAG(TAG_CLOSURE,$x),%s)
--   %c := %c+1
run (LDF f) = do
  e <- readVM envR
  push dataStack $ (DClosure f e)
  incrPC

-- AP - call function

-- Synopsis: pop a pointer to a CLOSURE cell off the data stack;
--           allocate an environment frame of size $n;
--           set the frame's parent to be the environment frame pointer
--             from the CLOSURE cell;
--           fill the frame's body with $n values from the data stack;
--           save the stack pointer, environment pointer and return address
--             to the control stack;
--           set the current environment frame pointer to the new frame;
--           jump to the code address from the CLOSURE cell;
-- Syntax:  AP $n
-- Example: AP 3      ; number of arguments to copy
-- Effect:
--   $x,%s := POP(%s)            ; get and examine function closure
--   if TAG($x) != TAG_CLOSURE then FAULT(TAG_MISMATCH)
--   $f := CAR_CLOSURE($x)
--   $e := CDR_CLOSURE($x)
--   $fp := ALLOC_FRAME($n)      ; create a new frame for the call
--   FRAME_PARENT($fp) := $e
--   $i := $n-1
--   while $i != -1 do           ; copy n values from the stack into the frame in reverse order
--   begin
--     $y,%s := POP(%s)
--     FRAME_VALUE($fp,$i) := $y
--     $i := $i-1
--   end
--   %d := PUSH(%e,%d)                     ; save frame pointer
--   %d := PUSH(SET_TAG(TAG_RET,%c+1),%d)  ; save return address
--   %e := $fp                             ; establish new environment
--   %c := $f                              ; jump to function

--   TAP - tail-call function
--
--    Synopsis: pop a pointer to a CLOSURE cell off the data stack;
--             allocate an environment frame of size $n;
--              set the frame's parent to be the environment frame pointer
--                from the CLOSURE cell;
--              fill the frame's body with $n values from the data stack;
--              set the current environment frame pointer to the new frame;
--              jump to the code address from the CLOSURE cell;
--    Syntax:  TAP $n
--    Example: TAP 3      ; number of arguments to copy
--    Effect:
--      $x,%s := POP(%s)            ; get and examine function closure
--      if TAG($x) != TAG_CLOSURE then FAULT(TAG_MISMATCH)
--      $f := CAR_CLOSURE($x)
--      $e := CDR_CLOSURE($x)
--      $fp := ALLOC_FRAME($n)      ; create a new frame for the call
--      FRAME_PARENT($fp) := $e
--      $i := $n-1
--      while $i != -1 do            ; copy n values from the stack into the frame in reverse order
--      begin
--        $y,%s := POP(%s)
--        FRAME_VALUE($fp,$i) := $y
--        $i := $i-1
--      end
--      %e := $fp                   ; establish new environment
--      %c := $f                    ; jump to function
--    Notes:
--      This instruction is the same as AP but it does not push a return address
--      The latest hardware revision optimizes the case where the environment
--      frame has not been captured by LDF and the number of args $n in the
--      call fit within the current frame. In this case it will overwrite the
--      frame rather than allocating a fresh one.

run (AP n) = runAP True n
run (TAP n) = runAP False n

-- RTN - return from function call

-- Synopsis: pop a stack pointer, return address and environment frame
--             pointer off of the control stack;
--           restore the stack and environment;
--           jump to the return address
-- Syntax:  RTN
-- Effect:
--   $x,%d := POP(%d)            ; pop return address
--   if TAG($x) == TAG_STOP then MACHINE_STOP
--   if TAG($x) != TAG_RET then FAULT(CONTROL_MISMATCH)
--   $y,%d := POP(%d)            ; pop frame pointer
--   %e := $y                    ; restore environment
--   %c := $x                    ; jump to return address
-- Notes:
--   Standard ABI convention is to leave the function return value on the
--   top of the data stack. Multiple return values on the stack is possible,
--   but not used in the standard ABI.

--   The latest hardware revision optimizes the deallocation of the
--   environment frame. If the environment has not been captured by LDF
--   (directly or indirectly) then it can be immediately deallocated.
--   Otherwise it is left for GC.

-- TODO: make use of last paragraph ^ -- dvyal

run RTN = do
  x <- pop controlStack
  case x of
    CStop -> return MachineStop
    CRet xv -> do
      y <- pop controlStack
      case y of
        CFrame yv -> do
          envR #= const yv
          controlR #= const xv
          continue
        _ -> fault "RTN couldn't get ret env frame from control register"
    _ -> fault "RTN, control mismatch, couldn't get CRet"

-- DUM - create empty environment frame
-- Synopsis: Prepare an empty frame;
--           push it onto the environment chain;
-- Syntax:  DUM $n
-- Example: DUM 3      ; size of frame to allocate
-- Effect:
--   $fp := ALLOC_FRAME($n)       ; create a new empty frame of size $n
--   FRAME_PARENT($fp) := %e      ; set its parent frame
--   %e := SET_TAG(TAG_DUM,$fp)   ; set it as the new environment frame
--   %c := %c+1
-- Notes:
--   To be used with RAP to fill in the frame body.
run (DUM n) = do
  e <- readVM envR
  fp <- makeDummyFrame n e
  envR #= const fp
  incrPC

-- RAP - recursive environment call function
-- Synopsis: pop a pointer to a CLOSURE cell off the data stack;
--           the current environment frame pointer must point to an empty
--             frame of size $n;
--           fill the empty frame's body with $n values from the data stack;
--           save the stack pointer, parent pointer of the current environment
--              frame and return address to the control stack;
--           set the current environment frame pointer to the environment
--             frame pointer from the CLOSURE cell;
--           jump to the code address from the CLOSURE cell;
-- Syntax:  RAP $n
-- Example: RAP 3      ; number of arguments to copy
-- Effect:
--   $x,%s := POP(%s)            ; get and examine function closure
--   if TAG($x) != TAG_CLOSURE then FAULT(TAG_MISMATCH)
--   $f := CAR_CLOSURE($x)
--   $fp := CDR_CLOSURE($x)
--   if FRAME_TAG(%e) != TAG_DUM then FAULT(FRAME_MISMATCH)
--   if FRAME_SIZE(%e) != $n then FAULT(FRAME_MISMATCH)
--   if %e != $fp then FAULT(FRAME_MISMATCH)
--   $i := $n-1
--   while $i != -1 do           ; copy n values from the stack into the empty frame in reverse order
--   begin
--     $y,%s := POP(%s)
--     FRAME_VALUE($fp,$i) := $y
--     $i := $i-1
--   end
--   $ep := FRAME_PARENT(%e)
--   %d := PUSH($ep,%d)                    ; save frame pointer
--   %d := PUSH(SET_TAG(TAG_RET,%c+1),%d)  ; save return address
--   FRAME_TAG($fp) := !TAG_DUM            ; mark the frame as normal
--   %e := $fp                             ; establish new environment
--   %c := $f                              ; jump to function

run (RAP n) = runRAP True n
run (TRAP n) = runRAP False n

-- STOP - terminate co-processor execution

-- Synopsis: terminate co-processor execution and signal the main proessor.
-- Syntax:  STOP
-- Effect:
--   MACHINE_STOP
-- Notes:
--   This instruction is no longer part of the standard ABI. The standard ABI
--   calling convention is to use a TAG_STOP control stack entry. See RTN.
run STOP = return MachineStop

-- ST - store to environment
--	Synopsis: pop a value from the data stack and store to the environment
--	Syntax:  ST $n $i
--	Example: ST 0 1
--	Effect:
--	  $fp := %e
--	  while $n > 0 do            ; follow chain of frames to get n'th frame
--	  begin
--		$fp := FRAME_PARENT($fp)
--		$n := $n-1
--	  end
--	  if FRAME_TAG($fp) == TAG_DUM then FAULT(FRAME_MISMATCH)
--	  $v,%s := POP(%s)           ; pop value from the data stack
--	  FRAME_VALUE($fp, $i) := $v ; modify i'th element of frame
--	  %c := %c+1
run (ST n i) = do
  fp <- readVM envR
  p <- n_times lookup_parent fp n
  dum <- isDummyFrame p
  when dum $ fault "FRAME_MISMATCH, ST needs not dummy frame"
  x <- pop dataStack
  liftIO $ writeIORef (frameData (e_vars p) i) x
  incrPC

-- DBUG - printf debugging
--	Synopsis: If tracing is enabled, suspend execution and raise a trace
--			  interrupt on the main processor. The main processor will read
--			  the value and resume co-processor execution. On resumption
--			  the value will be popped from the data stack. If tracing is not
--			  enabled the value is popped from the data stack and discarded.
--	Syntax:  DBUG
--	Effect:
--	  $x,%s := POP(%s)
--	  %c := %c+1
--	Notes:
--	  This is the formal effect on the state of the machine. It does
--	  also raise an interrupt but this has no effect on the machine state.
run DBUG = do
	x <- pop dataStack
	liftIO $ print x
	continue

--	BRK - breakpoint debugging
--	Synopsis: If breakpoint debugging is enabled, suspend execution and raise
--			  a breakpoint interrupt on the main processor. The main processor
--			  may inspect the state of the co-processor and can resume
--			  execution. If breakpoint debugging is not enabled it has no
--			  effect.
--	Syntax:  BRK
--	Effect:
--	  %c := %c+1
run BRK = do
	controlR #= succ
	return MachineDbgBreak

-- ************** Helpers
-- Synopsis: pop two integers off the data stack;
--           push result
--   $y,%s := POP(%s)
--   $x,%s := POP(%s)
--   if TAG($x) != TAG_INT then FAULT(TAG_MISMATCH)
--   if TAG($y) != TAG_INT then FAULT(TAG_MISMATCH)
--   $z := $x + $y
--   %s := PUSH(SET_TAG(TAG_INT,$z),%s)
--   %c := %c+1

runSEL pushRet t f = do
  x <- pop dataStack
  case x of
    DInt xv -> do
      c <- readVM controlR
      when pushRet $ push controlStack $ CJoin (succ c)
      case xv == 0 of
        True -> controlR #= const f
        False -> controlR #= const t
      continue
    _ -> fault "TAG_MISMATCH, SEL needs DInt"


runAP pushRet n = do
   x <- pop dataStack
   case x of
     DClosure f e -> do
       vars <- replicateM (fromIntegral n) (pop dataStack)
       fp <- makeFrame (reverse vars) e
       env <- readVM envR
       when pushRet $ push controlStack $ CFrame env
       c <- readVM controlR
       when pushRet $ push controlStack $ CRet (succ c)
       envR #= const fp
       controlR #= const f
       continue
     _ -> fault "TAG_MISMATCH, AP needs closure"

runRAP pushRet n = do
  x <- pop dataStack
  case x of
    DClosure f fp -> do
      e <- readVM envR
      dummy <- isDummyFrame e
      unless dummy $ fault "FRAME_MISMATCH, RAP needs dummy frame"
      unless (frameSize (e_vars e) == n) $ fault "FRAME_MISMATCH, variables dont match"
      -- FIXME: we dont compare environments
      mapM_ (\i -> pop dataStack >>= \v -> liftIO (writeIORef ((e_vars fp) ! fromIntegral i) v)) [n-1, n-2 .. 0]
      let ep = e_parent e
      when pushRet $ push controlStack $ CFrame ep
      c <- readVM controlR
      when pushRet $ push controlStack $ CRet (succ c)
      liftIO $ writeIORef (e_dummy fp) False
      envR #= const fp
      controlR #= const f
      continue
    _ -> fault "TAG_MISMATCH, RAP needs closure"

runIntOp :: (Value -> Value -> Value) -> GCCVM CmdResult
runIntOp iop = do
  y <- pop dataStack
  x <- pop dataStack
  case (x,y) of
    (DInt xv, DInt yv) -> do
      push dataStack (DInt $ iop xv yv)
      incrPC
    _ -> fault $ "tag mismatch in intop, " ++ show (x,y)

-- default run return value
continue = return MachineCont

-- increments PC
incrPC = do
  controlR #= succ
  continue

fault :: String -> GCCVM a
fault = liftIO . throwIO . AssertionFailed

-- General Helpers
n_times f val 0 = return val
n_times f val k = do val' <- f val
                     n_times f val' (k-1)

lookup_parent NullEnv = fault "frame doesn't have parent"
lookup_parent env     = return $ e_parent env
