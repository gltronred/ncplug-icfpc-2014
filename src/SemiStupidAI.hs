module SemiStupidAI where

import LambdaDSL hiding (not, and, or)
import qualified LambdaDSL as DSL
import HLDSL


scoreOf :: DSLast -> DSLast
scoreOf cell = iff (isWall cell)
                   (Const $ -5)
                   (iff (isFruitLoc cell)
                        (Const 20)
                        (iff (isPowerPill cell)
                             (Const 30)
                             (iff (isPill cell)
                                  (Const 10)
                                  (Const 0))))

scoreGhost :: DSLast -> DSLast -> DSLast
scoreGhost loc gsts = iff (isGhost loc gsts)
                          (Const $ -50)
                          (Const 0)

scoreWall :: DSLast -> DSLast
scoreWall cell = iff (isWall cell) (c $ -300) (c 0)

score :: Int -> (DSLast -> DSLast) -> DSLast -> DSLast -> DSLast -> DSLast
score depth dirFn loc wmap gsts = stdFoldl lam (scoreWall $ getMapCell (head cells) wmap) cells
  where
    cells = take depth $ iterate dirFn loc
    lam = lambda ["accscr", "lctn"] (add (add (scoreOf $ getMapCell (var "lctn") wmap) (scoreGhost (var "lctn") gsts)) (var "accscr"))

scoreUp3, scoreRight3, scoreDown3, scoreLeft3 :: DSLast -> DSLast -> DSLast -> DSLast
scoreUp3 loc wmap gsts = score 3 locUp loc wmap gsts

scoreRight3 loc wmap gsts = score 3 locRight loc wmap gsts

scoreDown3 loc wmap gsts = score 3 locDown loc wmap gsts

scoreLeft3 loc wmap gsts = score 3 locLeft loc wmap gsts


lookAhead3AI = script [
    defn "coward" ["s", "map", "man", "gsts"] [
      llet ["loc"] [getManLoc $ var "man"] [
        llet ["up", "right", "down", "left"] [locUp $ var "loc", locRight $ var "loc", locDown $ var "loc", locLeft $ var "loc"] [
          iff ((isWall $ getMapCell (var "up") (var "map")) `DSL.or` (isGhost (var "up") (var "gsts")))
              (iff ((isWall $ getMapCell (var "right") (var "map")) `DSL.or` (isGhost (var "right") (var "gsts")))
                   (iff ((isWall $ getMapCell (var "down") (var "map")) `DSL.or` (isGhost (var "down") (var "gsts")))
                        (iff ((isWall $ getMapCell (var "left") (var "map")) `DSL.or` (isGhost (var "left") (var "gsts")))
                             (Cons (var "s") dirUp)
                             (Cons (var "s") dirLeft))
                        (Cons (var "s") dirDown))
                   (Cons (var "s") dirRight))
              (Cons (var "s") dirUp)
        ]
      ]
    ],
    defn "step" ["s", "world"] [
      llet ["w"] [extractWorld "world"] [
        letn ["map", "man", "gsts"] [getMap $ var "w", getMan $ var "w", getGhosts $ var "w"] (\[wmap, man, gsts] ->
          lett "loc" (getManLoc man) (\loc ->
            letn ["scup", "scright", "scdown", "scleft"] [scoreUp3 loc wmap gsts, scoreRight3 loc wmap gsts, scoreDown3 loc wmap gsts, scoreLeft3 loc wmap gsts] (\[scup, scright, scdown, scleft] ->
              iff (gt scup scright)
                  (iff (gt scleft scdown)  -- scup > scright
                       (iff (gt scup scleft)  -- scleft > scdown
                            (Cons (var "s") dirUp)     -- scup > scleft > scdown
                            (iff (eq scup scleft)
                                 (call "coward" [var "s", wmap, man, gsts])
                                 (Cons (var "s") dirLeft)))  -- scleft > scup > scright
                       (iff (eq scleft scdown)
                            (iff (gt scup scdown)  -- scdown == scleft
                                 (Cons (var "s") dirUp)     -- scup > scdown == scleft
                                 (Cons (var "s") dirDown)) -- scdown == scleft >= scup > scright
                            (iff (gt scup scdown)  -- scdown > scleft
                                 (Cons (var "s") dirUp)     -- scup > scdown > scleft
                                 (iff (eq scup scdown)
                                      (call "coward" [var "s", wmap, man, gsts])
                                      (Cons (var "s") dirDown))))) -- scdown > scup > scright
                  (iff (gt scleft scdown)  -- scright >= scup
                       (iff (gt scright scleft) -- scleft > scdown
                            (Cons (var "s") dirRight)  -- scright > scleft > scdown
                            (iff (eq scright scleft)
                                 (call "coward" [var "s", wmap, man, gsts])
                                 (Cons (var "s") dirLeft)))  -- scleft > scright >= scup
                       (iff (gt scright scdown) -- scdown >= scleft
                            (Cons (var "s") dirRight)  -- scright > scdown >= scleft
                            (iff (eq scright scdown)
                                 (call "coward" [var "s", wmap, man, gsts])
                                 (Cons (var "s") dirDown)))) -- scdown > scright >= scup
            )
          )
        )
      ]
    ],
    Cons (Const 42) (var "step")
    ]


scoreUp5, scoreRight5, scoreDown5, scoreLeft5 :: DSLast -> DSLast -> DSLast -> DSLast
scoreUp5 loc wmap gsts = score 5 locUp loc wmap gsts

scoreRight5 loc wmap gsts = score 5 locRight loc wmap gsts

scoreDown5 loc wmap gsts = score 5 locDown loc wmap gsts

scoreLeft5 loc wmap gsts = score 5 locLeft loc wmap gsts

lookAhead5AI = script [
    defn "coward" ["s", "map", "man", "gsts"] [
      llet ["loc"] [getManLoc $ var "man"] [
        llet ["up", "right", "down", "left"] [locUp $ var "loc", locRight $ var "loc", locDown $ var "loc", locLeft $ var "loc"] [
          iff ((isWall $ getMapCell (var "up") (var "map")) `DSL.or` (isGhost (var "up") (var "gsts")))
              (iff ((isWall $ getMapCell (var "right") (var "map")) `DSL.or` (isGhost (var "right") (var "gsts")))
                   (iff ((isWall $ getMapCell (var "down") (var "map")) `DSL.or` (isGhost (var "down") (var "gsts")))
                        (iff ((isWall $ getMapCell (var "left") (var "map")) `DSL.or` (isGhost (var "left") (var "gsts")))
                             (Cons (var "s") dirUp)
                             (Cons (var "s") dirLeft))
                        (Cons (var "s") dirDown))
                   (Cons (var "s") dirRight))
              (Cons (var "s") dirUp)
        ]
      ]
    ],
    defun "selectDir" ["s", "map", "man", "gsts", "scup", "scright", "scdown", "scleft"] (\[s, wmap, man, gsts, scup, scright, scdown, scleft] ->
      iff (gt scup scright)
          (iff (gt scleft scdown)  -- scup > scright
               (iff (gt scup scleft)  -- scleft > scdown
                    (Cons s dirUp)     -- scup > scleft > scdown
                    (iff (eq scup scleft)
                         (call "coward" [s, wmap, man, gsts])
                         (Cons s dirLeft)))  -- scleft > scup > scright
               (iff (eq scleft scdown)
                    (iff (gt scup scdown)  -- scdown == scleft
                         (Cons s dirUp)     -- scup > scdown == scleft
                         (Cons s dirDown)) -- scdown == scleft >= scup > scright
                    (iff (gt scup scdown)  -- scdown > scleft
                         (Cons s dirUp)     -- scup > scdown > scleft
                         (iff (eq scup scdown)
                              (call "coward" [s, wmap, man, gsts])
                              (Cons s dirDown))))) -- scdown > scup > scright
          (iff (gt scleft scdown)  -- scright >= scup
               (iff (gt scright scleft) -- scleft > scdown
                    (Cons (var "s") dirRight)  -- scright > scleft > scdown
                    (iff (eq scright scleft)
                         (call "coward" [var "s", wmap, man, gsts])
                         (Cons (var "s") dirLeft)))  -- scleft > scright >= scup
               (iff (gt scright scdown) -- scdown >= scleft
                    (Cons (var "s") dirRight)  -- scright > scdown >= scleft
                    (iff (eq scright scdown)
                         (call "coward" [var "s", wmap, man, gsts])
                         (Cons (var "s") dirDown)))) -- scdown > scright >= scup
    ),
    defn "step" ["s", "world"] [
      llet ["w"] [extractWorld "world"] [
        letn ["map", "man", "gsts"] [getMap $ var "w", getMan $ var "w", getGhosts $ var "w"] (\[wmap, man, gsts] ->
          letn ["loc", "curdir"] [getManLoc man, getManDir man] (\[loc, curdir] ->
            iff (eq curdir (c 0)) -- moving up
                (call "selectDir" [var "s", wmap, man, gsts, add (c 5) $ scoreUp5 loc wmap gsts, scoreRight5 loc wmap gsts, scoreDown5 loc wmap gsts, scoreLeft5 loc wmap gsts])
                (iff (eq curdir (c 1)) -- moving right
                     (call "selectDir" [var "s", wmap, man, gsts, scoreUp5 loc wmap gsts, add (c 5) $ scoreRight5 loc wmap gsts, scoreDown5 loc wmap gsts, scoreLeft5 loc wmap gsts])
                     (iff (eq curdir (c 2)) -- moving down
                          (call "selectDir" [var "s", wmap, man, gsts, scoreUp5 loc wmap gsts, scoreRight5 loc wmap gsts, add (c 5) $ scoreDown5 loc wmap gsts, scoreLeft5 loc wmap gsts])
                          (iff (eq curdir (c 3)) -- moving left
                               (call "selectDir" [var "s", wmap, man, gsts, scoreUp5 loc wmap gsts, scoreRight5 loc wmap gsts, scoreDown5 loc wmap gsts, add (c 5) $ scoreLeft5 loc wmap gsts])
                               (call "selectDir" [var "s", wmap, man, gsts, scoreUp5 loc wmap gsts, scoreRight5 loc wmap gsts, scoreDown5 loc wmap gsts, scoreLeft5 loc wmap gsts]))))
          )
        )
      ]
    ],
    Cons (Const 42) (var "step")
    ]


