
module HungryAI where

import LambdaDSL hiding (not, and, or)
import qualified LambdaDSL as DSL
import HLDSL
import GCC.Compiler

-- Needs: stdlib
ffoldl :: DSLast -> DSLast -> DSLast -> DSLast
ffoldl fn zero lst = call "Foldl" [fn, zero, lst]

foldlTest = ffoldl (lamm ["acc", "v"] $ \[acc, v] -> v .: acc) nil (c 1 .: c 2 .: c 3 .: c 4)


-- Generates TONS of code, don't use it
bruteForceMap x_ y_ m cont = formap' x_ y_ where
  formap' x y =
    let cname = "cell" ++ "_" ++ (show x) ++ "_" ++ (show y)
        cpos = c x .: c y
    in
      llet [cname] [getMapCell cpos m] [
        iff (isPowerPill (var cname))
          (cont cpos)
          (case (x,y) of
            (1,1) -> cont (c 0 .: c 0)
            (1,_) -> formap' x_ (y-1)
            (_,_) -> formap' (x-1) y
          )
        ]

listletn_def  = defn "Listlen" ["ll-lst", "ll-acc"] [
    iff (atom $ var "ll-lst")
        (var "ll-acc")
        ((call "Listlen" [
           (cdr $ var "ll-lst"),
           (add (var "ll-acc") (1::IntValue))
         ]))
  ]

-- Length of the list
-- Needs listlen_def
listlen l = call "Listlen" [l,  Const 0]

-- Finds minimal list item. Returns tuple (measure, item)
-- Arg1: Starting measure
-- Arg2: Haskell-level predicate (item -> measure)
-- Arg3: list of items
listmin def pred l = ffoldl (lamm ["acc","s"] (\[acc,s] ->
    let
      measure = car acc
      item = cdr acc
      measure' = pred s
    in
      iff (measure'`lt`measure) (Cons measure' s) (acc)
  ))
  (Cons def (c 0)) l

-- (Width X Height)  of the map
-- Needs: listlen_def
getMapSize m = Cons (listlen (car m)) (listlen m)


-- Arg1: cell-checker predicate (see isPowerPill)
-- Arg2: list of lists of cells (see getMap)
-- Returns list of positions
scanMap chk m =
  let
    pos x y = Cons x y
    next x y l = Cons (pos x y) l

    getX = car . car
    getY = cdr . car
    getL = cdr

  in
    cdr $
      ffoldl (lamm ["st1", "xs1"] (\[st,xs] ->
        lett "s" (
          ffoldl (lamm ["st", "c"] (\[st, cl] ->
            let
              l = getL st
              x = getX st
              y = getY st
            in
              (next (x`add`(c 1)) y (
                iff (chk cl) ((pos x y) .: l) (l)
              ))
            )) st xs
          )
          (\s ->
            next (c 0) ((getY s)`add`(c 1)) (getL s)
          )
        )) (next (c 0) (c 0) nil) m

-- Distance between tho constants
dist a b = iff (a `lt` b) (b`sub`a) (a`sub`b)

-- Returns |a|
abss a = iff (a`gt`(c 0)) (a) ((c 0)`sub`a)

-- Rectangular distance as ((x1-x2) + (y1-y2))
distR l1 l2 = (dist (getLocY l1) (getLocY l2)) `add` (dist (getLocX l1) (getLocX l2))

-- Vector sub (l2 - l1)
vecdiff l1 l2 = ((getLocX l2)`sub`(getLocX l1)) .: ((getLocY l2) `sub` (getLocY l1))

-- +1 if a >=0;  -1 if < 0
sign a = iff (a`lt`(c 0)) (c (-1)) (c 1)

-- Returns direction from p1 to p2
vecdir p1 p2 = lett "diff" (vecdiff p1 p2) $ \diff ->
  let
    dx = car diff
    dy = cdr diff
  in
    -- iff ((abss dx)`gt`(abss dy))
    iff ((abss dx)`gt`(c 0))
      (iff (dx`gt`(c 0)) (dirRight) (dirLeft))
      (iff (dy`gt`(c 0)) (dirDown) (dirUp))

hungryAI =
    let
      for a b = map b a

      ifnot c a b = iff c b a

      findpill = car (var "s")

      pillloc = cdr (var "s")
    in
      script [
          listletn_def,

          defn "step" ["s", "world"] [
            lett "w" (extractWorld "world") $ \w ->
            lett "m" (getMap w) $ \m ->
            lett "lml" (getManLoc $ getMan w) $ \lml ->
            lett "any_pills" (lett "power_pills" (scanMap isPowerPill m) $ \power_pills ->
                                iff (atom power_pills)
                                  (scanMap isPill m)
                                  (power_pills)
                             ) $ \any_pills ->
            lett "tgt" (cdr $ listmin (c 999) (\c -> distR c lml) any_pills) $ \tgt ->
            lett "dir" (vecdir lml tgt) $ \dir ->

            (
              tgt
              -- dir
               -- (scanMap isPowerPill m)

              -- abss (c (-33))
              -- nearest pill
              -- listmin (c 999) (\c -> distR c (getManLoc lm)) (scanMap isPowerPill m)

             -- listmin (c 999) (\ploc -> distR ploc (getManLoc lm)) pills
             -- (c 1 .: c 3 .: c 4 .: c 4 .: c 5 .: nil)
              -- listmin (c 1000) id (c 11 .: c 3 .: c 4 .: c 4 .: c 5 .: nil)
              -- (scanMap isPowerPill m)
            ).:dir

              -- lett "x" (c 1) (\x1 -> lett "x" (c 3) (\x2 -> x1 .: dirUp))
              -- getManLoc lm .: dirUp
              -- dist (c 5) (c 3) .: dirUp
              -- Cons (foldlTest) dirUp
              -- Cons (scanMap isPowerPill (var "m")) dirUp
              -- Cons (getMapSize (var "m")) dirUp
              -- findInRange 50 10 (var "map") (\pos ->
              --   (Cons pos dirUp)
              -- )
          ],
          Cons (c 11 .: c 22) (var "step")
      ]

-- Guts of raycasting AI

raycast_depth = 5 :: IntValue -- num of cells to look into


data Dirs = DirLeft | DirRight | DirUp | DirDown deriving Show

locDir d = case d of
  DirLeft -> locLeft
  DirRight -> locRight
  DirUp -> locUp
  DirDown -> locDown

castDirs d = d : case d of
  DirLeft -> ver
  DirRight -> ver
  DirUp -> hor
  DirDown -> hor
 where hor = [DirLeft, DirRight]
       ver = [DirUp, DirDown]

init_state = dist .: wall_encountered .: ghost_encountered .: pills_eaten .: power_pills_eaten .: fruits_eaten
 where dist = c 0
       wall_encountered = c 0 -- ghosts or walls
       ghost_encountered = c 0 -- ghosts or walls
       pills_eaten = c 0
       power_pills_eaten = c 0
       fruits_eaten = c 0

account_toofar state = state
account_wall state = modifyNth (lambda ["x"] (c i_wall)) (c 1) state
account_ghost state = modifyNth (lambda ["x"] (c i_ghost)) (c 2) state
account_pill state = modifyNth (lambda ["x"] (add (var "x") (c 3))) (c 2) state
account_power_pill state = modifyNth (lambda ["x"] (add (var "x") (c 4))) (c 3) state
account_fruit state = modifyNth (lambda ["x"] (add (var "x") (c 5))) (c 4) state

state_too_far state = gt raycast_depth (nth (c 0) state)
state_fitness state = llet ["dist", "walls", "ghosts", "pills", "powerpills", "fruits"]
                        (map (\i -> nth (c i) state) [0..5]) [
                          iff (eq ghosts i_ghost) -- ghost
                            (c 0)
                            (add (add (pills `mul` (c 5)) (powerpills `mul` (c 100))) (fruits `mul` (c 200)))]
  where dist = var "dist"
        walls = var "walls"
        ghosts = var "ghosts"
        pills = var "pills"
        powerpills = var "powerpills"
        fruits = var "fruits"

def_getLoc = defn "GetLoc" ["loc"] [
  call "getSavedCell" [getLocY loc, getLocX loc]
  ]
 where loc = var "loc"

def_setLoc = defn "SetLoc" ["loc", "value"] [
  call "updateSavedCell" [getLocY loc, getLocX loc, value]
  ]
 where loc = var "loc"
       value = var "value"

getLoc l = call "GetLoc" [l]
setLoc l v = call "SetLoc" [l,v]

i_ghost :: IntValue
i_ghost = 6
-- ~ raycast :: Dir
def_raycast dir =
  defn ("Raycast_" ++ show dir) ["state", "loc"] [
   iff (state_too_far state) (account_toofar state)
   (lett "new_loc" (locDir dir loc) $ \new_loc ->
     lett "obj" (getLoc new_loc) $ \obj ->
       iff ( eq obj i_wall )
         (account_wall state)
         (iff ( eq obj i_ghost)
            (account_ghost state) -- TODO take direction into account
            (lett "new_state"
              (iff (eq obj i_pill)
                (account_pill state)
                (iff (eq obj i_power_pill)
                   (account_power_pill state)
                   (iff (eq obj i_fruit)
                    (account_fruit state)
                    state
                   )))
              $ \new_state ->
                  (llet ["res_dir1", "res_dir2", "res_dir3"]
                        (map (\d -> call ("Raycast_" ++ show d) [new_state, new_loc]) $ castDirs dir) [
                           let res_dir1 = var "res_dir1"
                               res_dir2 = var "res_dir2"
                               res_dir3 = var "res_dir3"
                           in (listmin (c 999999) (state_fitness) (list [res_dir1, res_dir2, res_dir3]))]))))]
 where state = var "state"
       loc = var "loc"


rayAI =
    let
      for a b = map b a
      ifnot c a b = iff c b a
      findpill = car (var "s")
      pillloc = cdr (var "s")
    in
      script [
          listletn_def,
          def_raycast DirLeft,
          def_raycast DirRight,
          def_raycast DirUp,
          def_raycast DirDown,
          def_getLoc,
          def_setLoc,

          defn "step" ["s", "world"] [
            lett "w" (extractWorld "world") $ \w ->
            lett "m" (getMap w) $ \m -> script [
              call "SaveMap" [m],
              lett "lml" (getManLoc $ getMan w) $ \lml ->
              lett "any_pills" (lett "power_pills" (scanMap isPowerPill m) $ \power_pills ->
                                 iff (atom power_pills)
                                 (scanMap isPill m)
                                 (power_pills)
                               ) $ \any_pills ->
              lett "tgt" (cdr $ listmin (c 999) (\c -> distR c lml) any_pills) $ \tgt ->
              lett "dir" (vecdir lml tgt) $ \dir ->

              (
                tgt
              ).:dir
              ]],
          Cons (c 11 .: c 22) (var "step")
      ]
