module GE.Compat where

import Control.Applicative hiding (Const)
import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Writer
import Data.List
import System.Random

import GE.Types
import GE.Sim
import LambdaDSL hiding(c)
import GCCVM
import GCC.Types
import GCC.Compiler


class TupleLike a where
  cons :: a -> a -> a
  uncons :: (a->x) -> (a->y) -> a -> (x,y)
  c :: Int -> a
  unc :: a -> Int
  lst :: [a] -> a

instance TupleLike DSLast where
  cons a b = Cons a b
  uncons fx fy (Cons a b) = (fx a, fy b)
  c i = Const (fromInteger $ toInteger i)
  unc (Const i) = i
  lst = list

instance TupleLike Datum where
  cons a b = DCons a b
  uncons fx fy (DCons a b) = (fx a, fy b)
  c i = DInt (fromInteger $ toInteger i)
  unc (DInt i) = fromInteger $ toInteger i
  lst = foldr DCons (DInt 0)

{-
The Ghosts' and Lambda-Man's direction is an enumeration:
  * 0: up;
  * 1: right;
  * 2: down;
  * 3: left.
-}

dirToDSL :: (TupleLike t) => Direction -> t
dirToDSL DUp     = c 0
dirToDSL DRight  = c 1
dirToDSL DDown   = c 2
dirToDSL DLeft   = c 3

dirFromDSL :: (TupleLike t) => t -> Direction
dirFromDSL = f . unc where
  f 0 = DUp
  f 1 = DRight
  f 2 = DDown
  f 3 = DLeft

cellToDSL :: (TupleLike t) => Cell -> t
cellToDSL cell =
  case cell of
    (Wall   )                  -> c 0
    (Cell _ Nothing)           -> c 1
    (Cell _ (Just Pill))       -> c 2
    (Cell _ (Just PowerPill))  -> c 3
    (Cell _ (Just (Fruit _)))   -> c 4
    {- FIXME: supply starting positions -}
    (Cell _ _)                   -> c 1

cellFromDSL :: (TupleLike t) => t -> Position  -> Cell
cellFromDSL ast p = f (unc ast) where
  f 0 = Wall
  f 1 = Cell p Nothing
  f 2 = Cell p (Just Pill)
  f 3 = Cell p (Just PowerPill)
  f 4 = Cell p (Just (Fruit 'f'))

posToDSL :: (TupleLike t) => Position -> t
posToDSL (x,y) = cons (c x) (c y)

posFromDSL :: (TupleLike t) => t -> Position
posFromDSL = uncons unc unc

worldToDSL :: (TupleLike t) => World -> t
worldToDSL w = cons m (cons lm_status (cons gst fruits)) where

  for a b = map b a

  (width,height) = levelSize w

  vit = c $ vitality w

  m = lst $ for [0..(height-1)] $ \y ->
        lst $ for [0..(width-1)] $ \x ->
          cellToDSL $ getCell (x,y) w

  lm_status = cons vit (cons loc (cons dir (cons lives scor))) where
    loc = posToDSL $ ppos $ lambdaMan w
    dir = dirToDSL $ pdir $ lambdaMan w
    lives = c $ nlives w
    scor = c $ score w

  gst = lst $ for (ghosts w) $ \g ->
          let loc = posToDSL $ ppos g
              dir = dirToDSL $ pdir g
          in cons vit (cons loc dir)

  fruits = c $ fruitTicks w

aiFromDSL :: (MonadIO m) => DSLast -> AI m (DSLast)
aiFromDSL ast w = return (st, runStep) where
  (Cons st step, env) = runScript (Progn [ast])
  runStep st w = do
    let Cons st' ndir = evalScriptWithEnv env (Progn [App step [st,worldToDSL w]])
    return (st', dirFromDSL ndir)

aiCompiledFromDSL :: DSLast -> AI IO Datum
aiCompiledFromDSL ast w = do
  secd <- return $ compile (Just stdlib) ast
  [DCons st clo] <- lucky <$> runGCC secd Nothing
  let
    step :: Datum -> World -> IO (Datum, Direction)
    step st w = do
      putStrLn "step"
      [DCons st' dir] <- lucky <$> runGCC secd (Just (clo, [st, worldToDSL w]))
      putStrLn (show st')
      return (st', dirFromDSL dir)
  do
    return (st, step)
  where
    lucky (RSuccess x) = x
    lucky x = error $ "lucky: runGCC fails wirh " ++ (show x)

{-
-- | Takes DSLast and the world @w (unused), returns pair of initial state and
-- step function. Initial state here is (Maybe Datum, VM).
--
-- FIXME: please, correct the code
aiCompiledFromDSL :: DSLast -> AI IO (Maybe Datum, Maybe VM)
aiCompiledFromDSL ast w = do
  secd <- return $ compile (Just stdlib) ast
  (retval, vm) <- lucky <$> runGCC secd Nothing
  -- ^ How should we interpret the stack after first run ?
  -- retval of first run is a pair (initial state, step function)
  -- we don't need stack itself
  return ((Nothing, vm), step)
  where
    lucky (RSuccess retval,vm) = (retval, vm)
    lucky _ = error "compile failed"

    step :: (Maybe Datum, Maybe VM) -> World -> IO ((Maybe Datum, Maybe VM), Direction)
    step (st, vm) w = do
      -- ^ What should we do with  (Just (st :: Datum)) from previous run?
      --   Should we pass it back to compile (see the next line) ?
      -- (Name "step") should be the second element of retval pair
      -- so we should not compile this Progn
      secd <- return $ compile (Just stdlib) (Progn [Call (Name "step") [undefined, worldToDSL w] ])
      -- ^ How to calculate the first argument of call to "step" ?
      -- it is a first element of pair of retval
      (retval, vm') <- lucky <$> runGCC secd vm
      let
        (DCons st' (DInt d)):_ = retval
      do
        return ((Just st', vm'), dirFromDSL (Const (fromIntegral d)))
-}

keyboardControlledAI :: (MonadIO m) => AI m ()
keyboardControlledAI _ =  return ((), kbd) where
  kbd st w = do
    c <- liftIO getChar
    case c of
      c |c`elem`"ah" -> return (st, DLeft)
        |c`elem`"sj" -> return (st, DDown)
        |c`elem`"wk" -> return (st, DUp)
        |c`elem`"dl" -> return (st, DRight)
        |otherwise -> return (st, DUp)

randomAI :: (MonadIO m) => AI m ()
randomAI _ = return ((), rnd) where
  rnd st w = do
    rnd <- liftIO $ randomIO
    return ((), dirFromDSL (Const (rnd`mod`4)))
