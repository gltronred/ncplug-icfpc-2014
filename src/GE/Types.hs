{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module GE.Types where

import Control.Monad
import Control.Applicative
import Control.Monad.Writer
import Control.Monad.State
import Data.Maybe
import Data.List as L
import Data.Map (Map(..))
import qualified Data.Set as S
import Data.Set (Set(..))
import qualified Data.Map as M

import Text.Printf

type Position = (Int,Int)
type Size = (Int,Int)

-- TODO: move Fruit to World (aka fruits :: Map Fruit Position)
data Loot = Pill | PowerPill | Fruit Char
  deriving(Show, Ord, Eq)

data Direction = DUp | DDown | DLeft | DRight
  deriving(Show,Eq)

adjucent DUp (x,y) = (x,y-1)
adjucent DDown (x,y) = (x,y+1)
adjucent DLeft (x,y) = (x-1,y)
adjucent DRight (x,y) = (x+1,y)

turnRight DUp = DRight
turnRight DRight = DDown
turnRight DDown = DLeft
turnRight DLeft = DUp

clockwize d = d:(clockwize (turnRight d))

degree d1 d2 = degree' 0 (clockwize d1) where
  degree' x (d:ds) | d==d2 = x
                   | otherwise = degree' (x+90) ds

angleAbs d1 d2 = min (degree d1 d2) (degree d2 d1)

data LambdaMan = LambdaMan Direction
  deriving(Show)

type Name = Char

data Cell = Wall | Cell {
    cpos :: Position
  , cloot :: (Maybe Loot)
  } deriving(Show)

isWall :: Cell -> Bool
isWall Wall = True
isWall _ = False

type Level = Map Position Cell

data Player = Player {
    pname :: Char
  , ppos :: Position
  , pdir :: Direction
  , pvisible :: Bool
  } deriving(Show)

data World = World {
    level :: Level
  , players :: Map Name Player
  , players_starting :: Map Name Player
  -- ^ Starting positions for players
  , utc :: UTC
  -- ^ Ultimate tick clock
  , frightMode :: Bool
  , score :: Int
  -- ^ Score points earned by the LM
  , eventPool :: Map UTC [Event]
  -- ^ Map of events: key is the absolute UTC
  , msgs :: [String]
  -- ^ Log (heads are newer than tails)
  , nlives :: Int
  }deriving(Show)

-- TODO: implement as separate counter
npills :: World -> Int
npills w = M.foldl' cnt 0 (level w) where
  cnt a (Cell _ (Just Pill)) = a+1
  cnt a _ = a

visibleGhostsAt :: Position -> World -> [Player]
visibleGhostsAt p w = filter (\(Player n _ _ v) -> (n /= '\\') && v) (playersAt p w)

ghosts :: World -> [Player]
ghosts = filter (\(Player n _ _ _) -> n`elem`['0'..'9']) . map snd . M.toList . players

err :: String -> World -> World
err s w = w{msgs = s:(msgs w)}

playersAt :: Position -> World -> [Player]
playersAt pos = L.filter (\p -> ppos p == pos) . map snd . M.toList . players

levelSize :: World -> Size
levelSize w = ((mx fst) + 1, (mx snd) + 1) where
  mx x = L.maximum $ map x $ map fst $ M.toList $ level $ w

getCell :: Position -> World -> Cell
getCell pos = fromJust . M.lookup pos . level

adjucents :: Position -> [(Direction,Position)]
adjucents p = map (\d -> (d,adjucent d p)) $ take 4 (clockwize DUp)

freeAdjucents ::  Position -> World -> [(Direction, Position)]
freeAdjucents p w = filter (\(_,p) -> not $ isWall $ getCell p w) (adjucents p)

vitality :: World -> Int
vitality w = calc $ M.filter (elem ExitFrightMode) $ eventPool w where
  calc m | M.null m = 0
         | otherwise = t' - t where
    t = utc w
    t' = fst $ M.findMin m

-- FIXME: implement fruitTicks
fruitTicks :: World -> Int
fruitTicks w = 0


data AdjacentsPattern = DeadEnd (Direction, Position)
                      | Tunnel [(Direction, Position)]
                      | Junction [(Direction, Position)]
                      deriving(Show)

adjacentsPattern :: Position -> World -> AdjacentsPattern
adjacentsPattern p w = let f = freeAdjucents p w in
  case length f of
    1 -> DeadEnd (f!!0)
    2 -> Tunnel f
    _ -> Junction f

minAngle :: Direction -> [(Direction,Position)] -> (Direction,Position)
minAngle d = head . sortBy (\(d1,_) (d2,_) -> (angleAbs d d1)`compare`(angleAbs d d2))

getPlayer :: Name -> World -> Maybe Player
getPlayer n =  listToMaybe . filter (\p -> pname p == n) . map snd . M.toList . players

setPlayer :: Player -> World -> World
setPlayer p w = w{ players = M.union (M.singleton (pname p) p) (players w) }

lambdaMan :: World -> Player
lambdaMan = fromJust . getPlayer '\\'

ghost :: Name -> World -> Maybe Player
ghost gn w
  | gn `elem` ['0'..'9'] = getPlayer gn w
  | otherwise = error "ghost: invalid ghost name"
  
moveLambdaMan :: Direction -> World -> (Player, World)
moveLambdaMan d w = flip runState w $ do
  lm <- lambdaMan <$> get
  let p' = (adjucent d (ppos lm))
  c <- getCell p' <$> get
  case c of
    Wall -> do
      modify $ err "LM forced to stop by the Wall"
      return lm
    Cell _ l -> do
      let lm' = lm{ppos = p', pdir = d}
      modify $ setPlayer lm'
      return lm'

moveLambdaMan' :: Direction -> World -> World
moveLambdaMan' d w = snd $ moveLambdaMan d w

-- | Returns Nothing if ghost doesn't exist
moveGhost :: Name -> Direction -> World -> (Maybe Player, World)
moveGhost gn dir_ w = flip runState w $ do
  let mg = getPlayer gn w
  case mg of
    Nothing -> do
      modify $ err $ "Attempt to move non-existent ghost " ++ (show gn)
      return Nothing
    Just g -> do
      dir <-  if angleAbs dir_  (pdir g) == 180 then do
                modify $ err $ "Ghost " ++ (show (pname g)) ++ " failed to turn 180 degrees"
                return (pdir g)
              else
                return dir_
      let move d = let g' = g{ppos=adjucent d (ppos g), pdir=d} in modify (setPlayer g') >> return (Just g')
      case adjacentsPattern (ppos g) w of
        DeadEnd (d', p') -> do
          modify $ err $ "Ghost " ++ (show gn) ++ " forced to go " ++ (show d') ++ " in the dead end"
          move d'
        Tunnel moves -> do
          let (d',p') = minAngle (pdir g) moves
          when (d' /= dir) $ do
            modify $ err $ "Ghost " ++ (show gn) ++ " forced to turn " ++ (show d') ++ " in tunnel"
          move d'
        Junction moves
          | dir`elem`(map fst moves) -> do
              move dir
          | otherwise -> do
              modify $ err $ "Ghost " ++ (show gn) ++ " selects invalid move " ++ (show dir)
              case (pdir g) == dir of
                False -> do
                  let (p',w') = moveGhost gn (pdir g) w
                  put w' >> return p'
                True -> do
                  modify $ err $ "Ghost " ++ (show gn) ++ " forced to turn right"
                  let (p',w') = moveGhost gn (turnRight $ pdir g) w
                  put w' >> return p'

moveGhost' :: Name -> Direction -> World -> World
moveGhost' n d w = snd $ moveGhost n d w


placeFruit :: Name -> Position -> World -> World
placeFruit fn pos w = w{level = M.adjust (\(Cell p x) -> Cell p (Just (Fruit fn))) pos (level w)}

removeFruitIfAny :: Position -> World -> World
removeFruitIfAny p w = w{ level = M.adjust rm p (level w)} where
  rm (Cell p (Just (Fruit _))) = Cell p Nothing
  rm c = c

clearCell l c w = w{ level = M.adjust rm (cpos c) (level w) } where
  rm c | cloot c == Just l = c{cloot = Nothing}
       | otherwise = error $ "clearCell: cell " ++(show c) ++ " doesn't contain " ++ (show l)

-- FIXME: add proper amount of scores
eatGhost :: Player -> World -> World
eatGhost p w =
  let
    p' = fromJust $ M.lookup (pname p) (players_starting w)
  in
    addScore 200 $
    err ("Ghost " ++ (show (pname p)) ++ " were eaten byt the LM") $
    w{players = M.adjust (\p -> p') (pname p) (players w)}

looseLife :: World -> World
looseLife w =
  err "LM have lost one life" $
  w{ nlives = (nlives w) - 1, players = players_starting w }

eatPill :: Cell -> World -> World
eatPill c = clearCell Pill c . addScore 10

eatPowerPill c = clearCell PowerPill c . addScore 50 . enterFrightMode

eatFruit c w = removeFruitIfAny (cpos c) $ addScore (levelScore w) w

enterFrightMode :: World -> World
enterFrightMode w = (schedule' (127*20 + (utc w)) ExitFrightMode w){ frightMode = True }

levelScore w = case nlevel w of
                 1   ->	100
                 2 	-> 	300
                 3 	-> 	500
                 4 	-> 	500
                 5 	-> 	700
                 6 	-> 	700
                 7 	-> 	1000
                 8 	-> 	1000
                 9 	-> 	2000
                 10 -> 	2000
                 11 -> 	3000
                 _ 	-> 	3000


addScore i w = w {score = (score w)+i}

assert True = return ()
assert False = error "Assert have triggered"

type UTC = Int

data Event = MoveLambdaMan
           | MoveGhost Name
           | AddFruit Name Position
           | RemoveFruit Position
           | ExitFrightMode
           | EOL
           deriving(Show, Eq, Ord)

-- Extracts sorted events
popEvent :: World -> ([Event], World)
popEvent w = let u = utc w in
  (sort $ M.findWithDefault [] u (eventPool w), w{eventPool = M.delete u (eventPool w)})

peekEvent :: World -> (UTC,[Event])
peekEvent = M.findMin . eventPool

deleteEvent :: UTC -> World -> World
deleteEvent u w = w{ eventPool = M.delete u (eventPool w)}

schedule' :: UTC -> Event -> World -> World
schedule' t e w = w{eventPool = M.unionWith mappend (eventPool w) (M.singleton t [e])}

schedule :: (MonadState World m) => UTC -> Event -> m ()
schedule t e = modify $ schedule' t e

nlevel :: World -> Int
nlevel wrld =
  let
    (w,h) = levelSize wrld
    area = w*h
    l = [((100 *(l-1)<area) && (area <= (100*l)),l)  | l <- [1..]]
  in snd $ head $ dropWhile (not . fst) l

eol wrld = let (w,h) = levelSize wrld in (127 * w * h * 16)

isEOL wrld = (utc wrld) >= eol wrld

loadFromFile :: FilePath -> IO World
loadFromFile f = do
  ls <- filter (\l -> (not (null (words l))) && (head l /= ';')) <$> lines <$> readFile f
  flip execStateT emptyWorld $ do
    forM_ (ls`zip`[0..]) $ \(l,y) -> do
      forM_ (l`zip`[0..]) $ \(c,x) -> do
        let p = (x,y)
        case c of
          '#' -> newcell (p, Wall)
          ' ' -> newcell (p, Cell p Nothing)
          '.' -> newcell (p, Cell p (Just Pill))
          'o' -> newcell (p, Cell p (Just PowerPill))
          '\\' -> do
            newcell (p, Cell p Nothing)
            modify $ setPlayer $ Player '\\' p DDown True
            modify $ schedule' (127) MoveLambdaMan
          'f' -> do
            newcell (p, Cell p Nothing)
            schedule (127*200) (AddFruit 'f' p)
            schedule (127*280) (RemoveFruit p)
          'F' -> do
            newcell (p, Cell p Nothing)
            schedule (127*400) (AddFruit 'F' p)
            schedule (127*480) (RemoveFruit p)
          gn | gn`elem`['0'..'9'] -> do
            newcell (p, Cell p Nothing)
            modify $ setPlayer $ Player gn p DDown True
            modify $ schedule' (130) (MoveGhost gn)
          x -> error ("unknown map element: " ++ show x)
    modify $ \w -> w{players_starting = players w}
    modify $ \w -> schedule' (eol w) EOL w

  where
    emptyWorld = World M.empty M.empty M.empty 1 False 0 (M.empty) (replicate 10 []) 3
    newcell (p,c) = modify $ \w -> w{level = M.insert p c (level w)}

drawWorld :: World -> String
drawWorld w = execWriter $ do
  let tswap (a,b) = (b,a)
  ll <- return (
    L.groupBy (\((_,y),_) ((_,y'),_) -> y==y') $
    L.sortBy (\(p1,_) (p2,_) -> (tswap p1)`compare`(tswap p2)) $
    M.toList  $
    level w)
  forM_ ll $ \l -> do
    forM_ l $ \(p,c) -> do
      case (c, playersAt p w) of
        (Wall   , _ )             -> tell "#"
        (Cell _ _ , [Player n _ _ _]) -> tell [n]
        (Cell _ _ , (a:as))         -> tell "*"
        (Cell _ (Just Pill) , [])   -> tell "."
        (Cell _ (Just PowerPill), []) -> tell "o"
        (Cell _ (Just (Fruit c)), []) -> tell "%"
        (Cell _ Nothing, [])        -> tell " "
    tell "\n"

drawStatus :: World -> String
drawStatus w = unlines $ execWriter $ do
  tell [printf "UTC:%d LIVES:%d VITA:%s SCORE:%d" (utc w) (nlives w) (show $ vitality w) (score w)]
  tell [(unlines $ map (\s -> "Status:   " ++ s ++ (replicate 50 ' ')) $ reverse $ take 3 (msgs w))]

