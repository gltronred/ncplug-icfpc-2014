{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}

module GE.Sim where

import Control.Applicative
import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Writer
import Data.List
import Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe
import GE.Types

{-
For LAMBDAMAN, main is a function with two arguments:

 1. the initial state of the world, encoded as below
 2. undocumented

It returns a pair containing:

 1. the initial AI state
 2. the AI step function (closure)
-}
type AI m st = (World -> {- -> Undocumented -} m (st, Step m st))


{-
The AI step function has two arguments:

 1. the current AI state
 2. the current state of the world, encoded as below

It returns a pair containing:

 1. the current AI state
 2. the move, encoded as below
-}
type Step m st = (st -> World -> m (st, Direction))


{- A bndle of AIs  -}
data SimArgs m l g = SimArgs { 
    lAI :: AI m l
  , gAI :: [AI m g]
  -- ^ Set of ghost AIs
  }

{- State of the simulator consists of world state and all the AI states -}
data SimState l g = SimState {
    world :: World
  , lm_st :: l
  , g_st :: Map Char g
  -- ^ Ghost State per Ghost
  } deriving(Show)

assignStates :: (Monad m) => World -> SimArgs m l g -> m (Step m l, Map Char (Step m g), SimState l g)
assignStates w (SimArgs _ []) = error "At least one ghost AI is required"
assignStates w sa = do
  (lm_state, lm_step) <- (lAI sa) w
  pairs <- forM (gAI sa) ($w)
  let
      ghost_pair_bindings = (sort $ map pname $ ghosts w)`zip`(cycle pairs)

      ghost_steps = M.fromList $ map (\(n,(_,step)) -> (n,step)) ghost_pair_bindings
      ghost_states = M.fromList $ map (\(n,(st,_)) -> (n,st)) ghost_pair_bindings
  do
    return (lm_step, ghost_steps, SimState w lm_state ghost_states)

data Reason
  = Win
  | Loose Bool {- True => EOL, False => no lives -}
  | Tick
  deriving(Show)

-- | Main simualtion function. Takes the AI bundle, the initial world, and the
-- inspect function allowing to print/query the state of the simulation
sim :: (Functor m, Monad m) => SimArgs m l g -> World -> (Reason -> SimState l g -> m ()) -> m ()
sim args w inspect_ = do
  (lstep, gsteps, ss) <- assignStates w args
  let

    wrld = world <$> get

    modworld f = modify $ \ss -> ss{world = f (world ss)}

    setgstate gn s = modify $ \ss -> ss {g_st = M.insert gn s (g_st ss)}

    getgstate gn = (fromJust . M.lookup gn . g_st) <$> get

    getlstate = get >>= \ss -> return (lm_st ss)

    setlstate s = modify $ \ss -> ss {lm_st = s}

    gstep gn = fromJust $ M.lookup gn gsteps

    inspect r = get >>= lift . inspect_ r

    stateful f = get >>= \s -> let (a,w') = f (world s) in put s{world = w'} >> return a

    tick = do
      inspect Tick

      t <- utc <$> wrld

      es <- stateful popEvent

      scheds <- execWriterT $ do
        forM_ es $ \e -> do
          case e of

            -- P1
            MoveLambdaMan -> do
              lift $ do
                w <- wrld
                ls <- getlstate
                (ls',dir) <- lift (lstep ls w)
                setlstate ls'
                modworld $ moveLambdaMan' dir
              tell [\eaten ->
                case eaten of
                  True -> modworld $ schedule' (t+137) e
                  False -> modworld $ schedule' (t+127) e ]

            MoveGhost gn -> do
              lift $ do
                w <- wrld
                gs <- getgstate gn
                (gs', dir) <- lift (gstep gn gs w)
                setgstate gn gs'
                modworld $ moveGhost' gn dir
              tell [\_ -> do
                fr <- frightMode <$> wrld
                case fr of
                  False -> modworld $ schedule' (t+130) e
                  True-> modworld $ schedule' (t+195) e ]

            -- P2
            AddFruit nm pos -> do
              lift $ modworld $ placeFruit nm pos

            RemoveFruit pos -> do
              lift $ modworld $ removeFruitIfAny pos

            ExitFrightMode -> do
              lift $ modworld $ \w -> w{frightMode = False}

            EOL -> do
              tell [\_ -> modworld $ schedule' (t+1) e]

      -- P3
      lm <- lambdaMan <$> wrld
      c <- getCell (ppos lm) <$> wrld
      eaten <- case c of
        -- P3.1
        Cell _ (Just Pill) -> do
          modworld (eatPill c)
          return True
        -- P3.2
        Cell _ (Just PowerPill) -> do
          modworld (eatPowerPill c)
          return True
        -- P3.3
        Cell _ (Just (Fruit fn)) -> do
          modworld (eatFruit c)
          return True
        _ -> do
          return False

      -- P4
      gs <- (visibleGhostsAt (ppos lm)) <$> wrld
      forM_ gs $ \g -> do
        fm <- frightMode <$> wrld
        case fm of
          True -> modworld (eatGhost g)
          False -> modworld (looseLife)

      -- P5
      n <- npills <$> wrld
      if (n==0) then
        inspect Win
      else do
        -- P6
        l <- nlives <$> wrld
        if (l==0) then
          inspect (Loose False)
        else do
          -- Re-scheduling LM event with appropriate timings
          forM_ scheds ($ eaten)

        -- P7
          eol <- isEOL <$> wrld
          if eol then
            inspect (Loose True)
          else do
            (t',_) <- peekEvent <$> wrld
            modworld $ \w -> w{utc = t'}
            tick

  do
    evalStateT tick ss

