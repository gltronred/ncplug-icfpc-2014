
import LambdaDSL (fact5, testRandom, stdlib)
import GCC.PrettyPrint (pretty)
import GCC.Compiler (compile)

main = putStrLn $ pretty $ compile (Just stdlib) testRandom

